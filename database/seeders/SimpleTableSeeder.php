<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Person;
use App\Models\Appointment;
use App\Models\Listing;
use App\Models\Project;
use App\Models\Affiliation;
use App\Models\ResearchArea;
use App\Models\Department;
use App\Models\StrategicResearchArea;
use App\Models\DepartmentResearchArea;
use Illuminate\Support\Facades\Storage;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class SimpleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Artisan::call('import:buildings');

        $faker = Faker::create();

        // Simple user account
        $user = User::firstOrCreate(['uwnetid' => 'test']);

        $strategic = StrategicResearchArea::factory()
            ->count(6)
            ->create();

        foreach (Department::all() as $d) {
            DepartmentResearchArea::factory()
                ->count($faker->numberBetween(4, 8))
                ->create(['department_id' => $d->id]);
        }

        $departments = Department::with('researchAreas')->get()->keyBy('id');

        // make a bunch of people
        $people = Person::factory()
            ->count(50)
            ->hasAppointments($faker->numberBetween(0, 5))
            ->create();

        foreach ($people as $person) {
            $photoUri = 'portraits/women/'. $person->id . '.jpg';
            if (!Storage::exists($photoUri)) {
                $photoUrl = 'https://randomuser.me/api/'.$photoUri;
                Storage::put($photoUri, file_get_contents($photoUrl));
            }
            $photoBytes = Storage::get($photoUri);
            $photo = \UWCoE\FileDB\File::createFromByteString(
                $photoBytes,
                ['mime' => 'image/jpg']
            );
            $person->files()->save($photo, ['usage' => 'photo']);

            foreach ($person->appointments as $appointment) {
                if ($departmentId = $appointment->department->id ?? null) {
                    $departmental = $departments[$departmentId]->researchAreas;
                    $listing = Listing::factory()
                        ->hasProjects($faker->numberBetween(0, 5))
                        ->hasAffiliations($faker->numberBetween(0, 5))
                        ->hasPersonalResearchAreas($faker->numberBetween(0, 10))
                        ->create([
                            'person_id' => $person->id,
                            'department_id' => $appointment->department->id,
                        ]);

                    $listing->strategicResearchAreas()->attach(
                        $strategic->random($faker->numberBetween(0, $strategic->count()))->pluck('id')
                    );
                    
                    $listing->personalResearchAreas()->attach(
                        $departmental->random($faker->numberBetween(0, $departmental->count()))->pluck('id')
                    );
                }
            }

            $person->publish();
        }
    }
}
