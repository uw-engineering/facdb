<?php

namespace Database\Factories;

use App\Models\ResearchArea;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentResearchAreaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'research_area_id' => ResearchArea::factory()->create()->id,
        ];
    }
}
