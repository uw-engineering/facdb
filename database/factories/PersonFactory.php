<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        return [
            'revised' => false,
            'first_name' => $faker->firstName,
            'middle_name' => $faker->optional()->firstName,
            'last_name' => $faker->lastName,
            'uwnetid' => $faker->userName,
            'employee_id' => $faker->randomNumber(8),
            'reg_id' => $faker->randomNumber(8),
            'email' => $faker->safeEmail,
            'phone' => $faker->phoneNumber,
            'fax' => $faker->optional()->phoneNumber,
            'office_building' => $faker->currencyCode,
            'office_number' => $faker->buildingNumber,
            'website_url' => $faker->optional()->url,
            'published_at' => null,
        ];
    }

    public function configure(): static
    {
        return $this->afterCreating(function (Person $person) {
            $person->disableRevisionField('hash');
            $person->hash = $person->generateHash();
            $person->save();
        });
    }
}
