<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ResearchAreaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()
                ->sentence($this->faker->numberBetween(2, 4)),
        ];
    }
}
