<?php

namespace Database\Factories;

use App\Models\Department;
use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;
        return [
            'person_id' => Person::factory(),
            'department_id' => Department::factory(),
            'bio' => $faker->text,
            'students' => $faker->optional()->words,
            'courses' => $faker->optional()->words,
            'cv_url' => $faker->optional()->url,
            'google_scholar' => $faker->optional()->url,
            'pubmed' => $faker->optional()->url,
            'post_doc' => null,
            'doctorate' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'masters' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'bachelors' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'prior_appointments' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'research_statement' => $faker->text,
            'publications' => $faker->optional()->sentences($faker->numberBetween(1, 20)),
            'awards' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'fellowships' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'memberships' => $faker->optional()->sentences($faker->numberBetween(1, 5)),
            'department_url' => $faker->optional()->url,
        ];
    }
}
