<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AffiliationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => $this->faker->word,
            'name' => $this->faker->sentence,
            'website' => $this->faker->optional()->url,
            'location' => $this->faker->optional()->address
        ];
    }
}
