<?php

namespace Database\Factories;

use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppointmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        $department_id = null;
        $other_department = null;
        if ($faker->boolean) {
            $department_id = Department::inRandomOrder()->first()->id;
        } else {
            $other_department = $faker->sentence($faker->numberBetween(1, 5));
        }
        return [
            'department_id' => $department_id,
            'other_department' => $other_department,
            'title' => $faker->sentence($faker->numberBetween(1, 5)),
            'endowed_title' => $faker->optional()->sentence($faker->numberBetween(1, 5)),
            'adjunct' => $faker->boolean,
        ];
    }
}
