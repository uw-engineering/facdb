<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentAndPersonalResearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('research_areas');
        Schema::create('research_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->unique();
        });
        Schema::create('strategic_research_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
        });
        Schema::create('department_research_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('departments');  
        });

        // foreach (config('research.strategic') as $name) {
        //     $area = App\Models\ResearchArea::firstOrCreate(['name' => $name]);
        //     App\Models\StrategicResearchArea::updateOrCreate(['research_area_id' => $area->id]);
        // }
        // foreach (config('research.department.aa') as $name) {
        //     $area = App\Models\ResearchArea::firstOrCreate(['name' => $name]);
        //     App\Models\DepartmentResearchArea::updateOrCreate([
        //         'research_area_id' => $area->id,
        //         'department_id' => 1
        //     ]);
        // }

        // DB::table('research_areas')->insert(array_map(function ($name) {
        //     return [
        //         'name' => $name
        //     ];
        // }, config('research.strategic')));

        // DB::table('research_areas')->insert(array_map(function ($name) {
        //     return [
        //         'name' => $name,
        //         'department_id' => 1
        //     ];
        // }, config('research.department.aa')));

        Schema::create('listing_strategic_research_area', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
        });

        Schema::create('listing_department_research_area', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
        });

        Schema::create('listing_personal_research_area', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_strategic_research_area');
        Schema::drop('listing_department_research_area');
        Schema::drop('listing_personal_research_area');
        Schema::drop('department_research_areas');
        Schema::drop('strategic_research_areas');
        
        Schema::drop('research_areas');
        Schema::create('research_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');

            $table->boolean('strategic')->default(false);
            $table->string('name');
        });
    }
}
