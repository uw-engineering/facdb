<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeparateListingsAndAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn(['published', 'other_department', 'position', 'endowed', 'endowed_title', 'other']);
            
            $table->unique(['person_id', 'department_id']);

            $table->text('department_url')->nullable();
        });

        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('department_id')->unsigned()->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('people');
            $table->string('other_department')->nullable();
            $table->string('title')->nullable();
            $table->string('endowed_title')->nullable();
            $table->boolean('adjunct')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appointments');

        Schema::table('listings', function (Blueprint $table) {
            $table->boolean('published');
            $table->string('other_department')->nullable();
            $table->string('position')->nullable();
            $table->boolean('endowed')->default(false);
            $table->string('endowed_title')->nullable();
            $table->text('other')->nullable();

            $table->dropColumn('department_url');
        });
    }
}
