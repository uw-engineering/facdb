<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->boolean('published');

            $table->integer('person_id')->unsigned();
            $table->integer('department_id')->unsigned()->nullable();

            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('department_id')->references('id')->on('departments');

            $table->string('other_department')->nullable();

            $table->string('position')->nullable();
            $table->boolean('endowed')->default(false);
            $table->string('endowed_title')->nullable();

            $table->text('bio')->nullable();
            $table->text('other')->nullable();

            $table->text('projects')->nullable();
            $table->text('students')->nullable();
            $table->text('fellows')->nullable();
            $table->text('courses')->nullable();

            $table->string('cv_url')->nullable();
            $table->string('google_scholar')->nullable();
            $table->string('pubmed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
