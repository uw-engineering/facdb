<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimplifyPublications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->text('publications')->nullable();
        });

        Schema::drop('publications');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');

            $table->text('text');
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn('publications');
        });
    }
}
