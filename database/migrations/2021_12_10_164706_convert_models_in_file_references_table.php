<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class ConvertModelsInFileReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('file_references')
            ->update(['file_reference_type' => 'App\\Models\\Person']);
    }

}
