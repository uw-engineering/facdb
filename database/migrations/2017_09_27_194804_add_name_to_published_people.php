<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToPublishedPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('published_people', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->index('name');

            $table->string('last_name')->nullable();
            $table->index('last_name');
        });

        Artisan::call('listings:refresh');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('published_people', function (Blueprint $table) {
            $table->dropIndex(['name']);
            $table->dropColumn('name');

            $table->dropIndex(['last_name']);
            $table->dropColumn('last_name');
        });
    }
}
