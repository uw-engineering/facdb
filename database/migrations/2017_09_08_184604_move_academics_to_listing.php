<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveAcademicsToListing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->text('post_doc')->nullable();
            $table->text('doctorate')->nullable();
            $table->text('masters')->nullable();
            $table->text('bachelors')->nullable();
            $table->text('prior_appointments')->nullable();
        });

        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn(['post_doc', 'doctorate', 'masters', 'bachelors', 'prior_appointments']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->string('post_doc')->nullable();
            $table->string('doctorate')->nullable();
            $table->string('masters')->nullable();
            $table->string('bachelors')->nullable();
            $table->text('prior_appointments')->nullable();
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn(['post_doc', 'doctorate', 'masters', 'bachelors', 'prior_appointments']);
        });
    }
}
