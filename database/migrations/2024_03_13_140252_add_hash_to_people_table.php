<?php

use App\Models\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('people', function (Blueprint $table) {
            $table->string('hash')->nullable()->after('id');
        });

        foreach (Person::all() as $person) {
            $person->setRevisionEnabled(false);
            $person->hash = $person->generateHash();
            $person->save();
        }
    }

    public function down(): void
    {
        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
    }
};
