<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlHashToPublishedPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('published_people', function (Blueprint $table) {
            $table->string('hash')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('published_people', function (Blueprint $table) {
            $table->dropUnique(['hash']);
            $table->dropColumn('hash');
        });
    }
}
