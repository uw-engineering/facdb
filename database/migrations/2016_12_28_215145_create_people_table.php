<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->boolean('published')->default(false);

            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('preferred_name')->nullable();

            $table->string('uwnetid')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('reg_id')->nullable();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('office_number')->nullable();
            $table->string('office_building')->nullable();
            $table->string('website_url')->nullable();
            $table->string('title')->nullable();

            $table->text('prior_appointments')->nullable();
            $table->string('post_doc')->nullable();
            $table->string('doctorate')->nullable();
            $table->string('masters')->nullable();
            $table->string('bachelors')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
