<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliations', function (Blueprint $table) {
            $table->renameColumn('title', 'name');
        });

        Schema::table('affiliations', function (Blueprint $table) {
            $table->string('website')->nullable()->change();
            $table->string('location')->nullable();
            $table->string('phone')->nullable();
            $table->dropColumn('description');
        });

        Schema::create('affiliation_research_area', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('affiliation_id')->unsigned();
            $table->foreign('affiliation_id')
                ->references('id')
                ->on('affiliations')
                ->onDelete('cascade');
            $table->integer('research_area_id')->unsigned();
            $table->foreign('research_area_id')->references('id')->on('research_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliations', function (Blueprint $table) {
            $table->renameColumn('name', 'title');
        });

        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropColumn(['location', 'phone']);
            $table->text('description');
            $table->string('website')->change();
        });

        Schema::drop('affiliation_research_area');
    }
}
