<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimplifyAwards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('honors');
        
        Schema::table('listings', function (Blueprint $table) {
            $table->text('awards')->nullable();
            $table->text('fellowships')->nullable();
            $table->text('memberships')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('honors', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')->references('id')->on('listings');

            $table->string('type');
            $table->text('text');
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn('awards');
            $table->dropColumn('fellowships');
            $table->dropColumn('memberships');
        });
    }
}
