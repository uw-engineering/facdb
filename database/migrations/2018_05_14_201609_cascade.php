<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_published_person', function (Blueprint $table) {
            $table->dropForeign(['published_person_id']);
        });

        Schema::table('department_published_person', function (Blueprint $table) {
            $table->foreign('published_person_id')
                ->references('id')->on('published_people')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_published_person', function (Blueprint $table) {
            $table->dropForeign(['published_person_id']);
        });

        Schema::table('department_published_person', function (Blueprint $table) {
            $table->foreign('published_person_id')
                ->references('id')->on('published_people');
        });
    }
}
