<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cascades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')->on('people')
                ->onDelete('cascade');
        });


        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign(['department_id']);
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')->on('departments')
                ->onDelete('set null');
        });


        Schema::table('listings', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')->on('people')
                ->onDelete('cascade');
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->dropForeign(['department_id']);
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')->on('departments')
                ->onDelete('set null');
        });

        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
        });
        Schema::table('affiliations', function (Blueprint $table) {
            $table->foreign('listing_id')
                ->references('id')->on('listings')
                ->onDelete('cascade');
        });

        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->dropForeign(['affiliation_id']);
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->foreign('affiliation_id')
                ->references('id')->on('affiliations')
                ->onDelete('cascade');
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas')
                ->onDelete('cascade');
        });


        Schema::table('listing_department_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_department_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas')
                ->onDelete('cascade');
        });
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas')
                ->onDelete('cascade');
        });
        Schema::table('listing_strategic_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_strategic_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas')
                ->onDelete('cascade');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->foreign('listing_id')
                ->references('id')->on('listings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')->on('people');
        });


        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign(['department_id']);
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')->on('departments');
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')->on('people');
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->dropForeign(['department_id']);
        });
        Schema::table('listings', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')->on('departments');
        });


        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
        });
        Schema::table('affiliations', function (Blueprint $table) {
            $table->foreign('listing_id')
                ->references('id')->on('listings');
        });


        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->dropForeign(['affiliation_id']);
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->foreign('affiliation_id')
                ->references('id')->on('affiliations');
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('affiliation_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas');
        });


        Schema::table('listing_department_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_department_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas');
        });
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas');
        });
        Schema::table('listing_strategic_research_area', function (Blueprint $table) {
            $table->dropForeign(['research_area_id']);
        });
        Schema::table('listing_strategic_research_area', function (Blueprint $table) {
            $table->foreign('research_area_id')
                ->references('id')->on('research_areas');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->foreign('listing_id')
                ->references('id')->on('listings');
        });
    }
}
