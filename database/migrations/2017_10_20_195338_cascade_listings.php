<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CascadeListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
            $table->foreign('listing_id')
                ->references('id')
                ->on('listings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listing_personal_research_area', function (Blueprint $table) {
            $table->dropForeign(['listing_id']);
            $table->foreign('listing_id')
                ->references('id')
                ->on('listings');
        });
    }
}
