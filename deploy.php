<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'facdb');

// Project repository
set('repository', 'ssh://git@shinji.engr.washington.edu:7999/faclook/facdb.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Deploy Path
set('deploy_path', '/var/www/apps/{{application}}');

// Hosts
inventory('hosts.yml');
    
// Tasks

// task('build', function () {
//     $stage = input()->getArgument('stage');
    
//     run('composer install');
//     run('yarn');
//     run('npm run ' . $stage);
//     run('rm -r node_modules');
//     // ...
// })->local();

// task('upload', function () {
//     upload(__DIR__, '{{release_path}}');
// });

// task('release', [
//     'deploy:prepare',
//     'deploy:release',
//     'upload',
//     'deploy:shared',
//     'deploy:writable',
//     'deploy:symlink',
// ]);

// task('deploy', [
//     'build',
//     'release',
//     'cleanup',
//     'success'
// ]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

