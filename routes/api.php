<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use UWCoE\FileDB\File;
use App\Models\PublishedPerson;
use App\Models\Photo;
use App\Models\StrategicResearchArea;
use App\Models\Department;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('person/{hash}', function ($hash) {
    // name will be formatted as a url component
    $person = PublishedPerson::where('hash', $hash)->first();
    if ($person) {
        return $person;
    }
    abort(404);
});

Route::get('research/strategic', function () {
    return StrategicResearchArea::with('researchArea')->get()->pluck('researchArea.name');
});

Route::get('research/{dept}', function ($dept) {
    return Department::with('researchAreas')
        ->where('abbreviation', 'like', $dept)
        ->first()->researchAreas->pluck('name');
});

Route::get('people', function (Request $request) {

    $result = PublishedPerson::with('personObject', 'departments')->orderBy('last_name', 'asc');

    $pageSize = 20;
    if ($request->has('page_size')) {
        $pageSize = (int)$request->input('page_size');
    }

    if ($request->input('search')) {
        $result->where('name', 'like', '%'.$request->input('search').'%');
    }

    if ($request->has('area')) {
        $result->where('research', 'like', '%'.$request->input('area').'%');
    }

    if ($request->input('dept')) {
        $result->whereHas('departments', function ($query) use ($request) {
            $query
                ->where('abbreviation', $request->input('dept'))
                ->where('adjunct', false);
        });
    }

    if ($request->has('letter')) {
        $result->where('last_name', 'like', $request->input('letter') . '%');
    }

    if (!$request->has('show_emeritus')) {
        $result->whereDoesntHave('personObject.appointments', function ($query) {
            $query->where('title', 'LIKE', '%emeritus%');
        });
    }

    if (!$request->has('show_affiliates') && $request->input('dept')) {
            $result->whereDoesntHave('personObject.appointments', function ($query) use ($request) {
                $query->where('affiliate', true);

                $dept = DB::table('departments')
                        ->where('abbreviation', $request->input('dept'))
                        ->first();
                if ($dept) {
                    $query->where('department_id', $dept->id);
                }
            });
    }
    return $result->paginate($pageSize)->appends($request->all());
});

Route::get('people/department/{department}', function (Department $department) {
    return $department->publishedPeople()->paginate(20);
});

/* Files */

//File::registerRouteBinding();
Route::get('file/{file}', function (File $file) {
    return $file->response();
})->name('file');

Route::get('default-photo/{width?}/{height?}', function () {
    return response()->file(resource_path('/assets/images/default_photo.png'));
})->name('default-photo');



Route::get('profile-photo/{hash}/{width?}/{height?}', function (string $hash, int $width = 0, int $height = 0) {
    $pub = PublishedPerson::where('hash', $hash)->first();
    if ($pub && $pub->personObject->photo) {
        return $pub->personObject->photo->imageResponse($width, $height);
    } else {
        abort(404);
    }
})->name('profile-photo');

Route::get('cv/{hash}', function (string $hash) {
    $pub = PublishedPerson::where('hash', $hash)->first();
    if ($pub && $pub->personObject->photo) {
        return $pub->personObject->cv->response();
    } else {
        abort(404);
    }
})->name('cv');

//Photo::registerRouteBinding('photo', 'people');
Route::get('photo/{photo}/{width?}/{height?}', function (Photo $photo, int $width = 0, int $height = 0) {
    return $photo->imageResponse($width, $height);
})->name('photo');
