<?php

use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('netid', 'can:use-admin')->group(function () {

    // List Page
    // Route::get('/', [AdminController::class, 'list'])->name('list');

    Route::any('user/publish/{person}', 'Person\HandlePublish')->name('person.publish');
    Route::any('person/add', 'Person\HandleAdd')->name('person.add');
    Route::any('person/{person}/delete', 'Person\HandleDelete')->name('person.delete');
    Route::any('person/{person}/unpublish', 'Person\HandleUnpublish')->name('person.unpublish');
    // Route::get('person/{person}/revisions', 'Person\HandleRevisions')->name('person.revisions');

    // Research Areas
    Route::group(['as' => 'research.', 'prefix' => 'research'], function () {
        Route::any('select', 'Research\HandleSelect')->name('select');
        Route::any('department/{department}', 'Research\HandleDepartment')->name('department');
        Route::any('strategic', 'Research\HandleStrategic')->name('strategic');
    });
});

Route::middleware('netid')->get('edit', function () {
    $person = App\Models\Person::firstOrNew(['uwnetid' => Auth::user()->uwnetid]);
    return View::make('form')->with('person', $person);
});

Route::name('history')->get('history', function () {
    $person = App\Models\Person::with('revisionHistory')->first();
    //return $person->toJson();
    return view('admin.person', [
        'person' => $person
    ]);
});
