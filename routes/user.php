<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "user" middleware group. Now create something great!
|
*/

// Require basic netid auth for all forms
Route::group(['middleware' => 'netid'], function () {
    Route::any('', 'HandleWelcome')->name('welcome');
});

Route::group(['middleware' => 'netid', 'prefix' => 'edit/{person}'], function () {

    // Basic
    Route::group(['as' => 'basic.'], function () {
        Route::any('basic/name', 'Basic\HandleName')->name('name');
        Route::any('basic/photo', 'Basic\HandlePhoto')->name('photo');
        Route::any('basic/contact', 'Basic\HandleContact')->name('contact');
        Route::any('basic/admin', 'Basic\HandleAdmin')->name('admin');
        Route::any('basic/next', 'Basic\HandleNext')->name('next');
    });

    // Appointments
    Route::group(['as' => 'appointment.', 'prefix' => 'appointment'], function () {
        Route::any('list', 'Appointment\HandleList')->name('list');
        Route::any('add', 'Appointment\HandleAdd')->name('add');
        Route::any('{appointment}/edit', 'Appointment\HandleEdit')->name('edit');
        Route::any('{appointment}/delete', 'Appointment\HandleDelete')->name('delete');
    });

    // Listings
    Route::group(['as' => 'listing.', 'prefix' => 'listing'], function () {
        Route::any('list', 'Listing\HandleList')->name('list');
        Route::any('add', 'Listing\HandleAdd')->name('add');
        Route::any('{listing}/edit', 'Listing\HandleEdit')->name('edit');
        Route::any('{listing}/delete', 'Listing\HandleDelete')->name('delete');
    });

    // Listing Group
    Route::prefix('listing/{listing}')->group(function() {

        // History
        Route::group(['as' => 'history.', 'prefix' => 'history'], function () {
            Route::any('cv', 'History\HandleCv')->name('cv');
            Route::any('academics', 'History\HandleAcademics')->name('academics');
            Route::any('appointments', 'History\HandleAppointments')->name('appointments');
            Route::any('bio', 'History\HandleBio')->name('bio');
        });

        // Research
        Route::group(['as' => 'research.', 'prefix' => 'research'], function () {
            Route::any('strategic', 'Research\HandleStrategic')->name('strategic');
            Route::any('department', 'Research\HandleDepartment')->name('department');
            Route::any('personal', 'Research\HandlePersonal')->name('personal');
            Route::any('statement', 'Research\HandleStatement')->name('statement');
        });

        // Affiliations
        Route::any('affiliations', 'Affiliation\HandleList')->name('affiliation.list');
        Route::any('affiliations/add', 'Affiliation\HandleAdd')->name('affiliation.add');
        Route::group(['as' => 'affiliation.', 'prefix' => 'affiliation/{affiliation}'], function () {
            Route::any('edit', 'Affiliation\HandleEdit')->name('edit');
            Route::any('delete', 'Affiliation\HandleDelete')->name('delete');        
        });

        // Publications
        Route::group(['as' => 'publication.', 'prefix' => 'publication'], function () {
            Route::any('google', 'Publication\HandleGoogle')->name('google');
            Route::any('list', 'Publication\HandleList')->name('list');
        });

        // Work
        Route::any('projects', 'Work\HandleProjectList')->name('work.project.list');
        Route::any('project/add', 'Work\HandleProjectAdd')->name('work.project.add');
        Route::group(['as' => 'work.project.', 'prefix' => 'project/{project}'], function () {
            Route::any('edit', 'Work\HandleProjectEdit')->name('edit');
            Route::any('delete', 'Work\HandleProjectDelete')->name('delete');        
        });
        Route::any('work/courses', 'Work\HandleCourses')->name('work.courses');
        Route::any('work/students', 'Work\HandleStudents')->name('work.students');

        // Honors
        Route::group(['as' => 'honors.', 'prefix' => 'honors'], function () {
            Route::any('awards', 'Honors\HandleAwards')->name('awards');
            Route::any('fellowships', 'Honors\HandleFellowships')->name('fellowships');
            Route::any('memberships', 'Honors\HandleMemberships')->name('memberships');
        });
    });
});
