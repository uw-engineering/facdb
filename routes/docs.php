<?php

$pages = [
    'introduction',
    'authentication',
    'people',
    'person',
    'research',
    'files',
    'changelog'
];

Route::view('introduction', 'docs.introduction', ['pages' => $pages])->name('introduction');

Route::get('', function () {
    return redirect()->route('docs.introduction');
});

Route::get('{path?}', function (Request $request, $path) use ($pages) {
    try {
        if ($path) {
            return view('docs.' . $path, ['pages' => $pages]);
        }
    } catch (InvalidArgumentException $e) {
        //
    }
    return redirect()->route('docs.introduction');
});
