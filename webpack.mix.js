const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js('resources/assets/js/app.js', 'public/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css')
    .setResourceRoot(process.env.APP_URL + '/')
    .copy('node_modules/summernote/dist', 'public/vendor/summernote')
    .copy('node_modules/codemirror/lib', 'public/vendor/codemirror')

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
