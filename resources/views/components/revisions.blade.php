@props(['record'])

@foreach ($record->revisions as $revision)
    <p>
        <span style="background: rgba(100, 100, 100, 0.3)">&nbsp;{{ $revision->fieldName() }}&nbsp;</span>
        changed from
        <span style="background: rgba(200,0,0,0.3)">&nbsp;<s>{!! $revision->old_value ?? "<i>null</i>" !!}</s>&nbsp;</span>
        to
        <span style="background: rgba(0,200,0,0.3)">&nbsp;{{ $revision->new_value }}&nbsp;</span>
    </p>
@endforeach
