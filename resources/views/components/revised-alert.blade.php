<div class="alert alert-light">
  <h3>Are you done editing?</h3>
  <p>
    Once you are ready, 
    <a href="mailto:webhelp@engr.uw.edu">send us an email</a>
    so we can publish your changes. Thank you!
</div>
