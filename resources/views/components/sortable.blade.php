<div class="form-group row mb-4">
  
  @if ($label)
    <label class="col-sm-3 col-form-label">{{ $label }}</label>
  @endif
  
  <ul id="{{ $id }}" class="{{ $label ? 'col-sm-9' : 'col' }} sortable-list">
  
    @foreach ($list as $element)
      <li class="input-group dynamic-text-input">

        <input type="text" 
          class="form-control"
          name="{{ $name }}[]"
          value="{{ $element ?? '' }}"
        >

        @if (count($list) != 1)
          <div class="input-group-append handle">
            <a class="btn btn-secondary" type="button">
              <i class="fas fa-arrows-alt fa-fw" aria-hidden="true"></i>
            </a>
          </div>
        @endif

        @if (count($list) != 1)
          <div class="input-group-append dynamic-text-input-remove">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-minus fa-fw" aria-hidden="true"></i>
            </button>
          </div>
        @endif

      </li>
    @endforeach

  </ul>

  <div class="col-12 justify-content-end d-flex">
    <button class="btn btn-secondary btn-sm add-more" type="button"
      style="{{ ($list[0] ?? false) ? '' : 'display: none;' }}">
      Add more
    </button>
  </div>

</div>

<script>
  window.addEventListener('DOMContentLoaded', (event) => {
    Sortable.create(document.getElementById('{{ $id }}'), {
      handle: '.handle',
    })
  })
</script>
