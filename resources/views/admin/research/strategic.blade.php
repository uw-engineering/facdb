@extends('admin.dashboards.basic')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Strategic Research Areas</h2>
          <h6 class="card-subtitle text-muted">
            With great power comes great responsibility
          </h6>

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post">
            {{ csrf_field() }}
            
            <x-sortable label="Research Area" name="research" :list="$strategicResearchAreas->pluck('name')"/>

            <div class="form-group row">
              <div class="col">
                <a href="{{ route('admin.research.select') }}" class="btn btn-secondary">Cancel</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
