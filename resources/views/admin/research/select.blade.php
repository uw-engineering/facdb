@extends('admin.dashboards.basic')

@section('main')

 <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Research Areas</h2>

          <form method="post">
            {{ csrf_field() }}

            <h3>Modify Department Research Areas</h3>
            <div class="form-group row">
              <label for="department" class="col-sm-3 col-form-label">Department</label>
              <div class="col-sm-9">
                <select name="department" class="custom-select">
                  <option selected>Department</option>
                  @foreach ($departments as $department)
                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-3"></div>
              <div class="col-sm-9">
                <button type="submit" class="btn btn-primary">Go</button>
              </div>
            </div>

          </form>

          <h3>Modify Strategic Research Areas</h3>

          <div class="form-group row">
            <div class="col">
              <a href="{{ route('admin.research.strategic') }}" class="btn btn-primary">Go</a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
 
@endsection
