@extends('admin.dashboards.basic')
@section('main')

  <form id="search-form">
    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <input name="search" type="text" placeholder="Search Faculty" class="form-control" value="{{ $search }}">
        </div>
      </div>
    </div>
  </form>

  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th></th>
          <th>  
            <a href={{ route('admin.list', ['sort' => 'last_name', 'order' => $order, 'flip'=> strcmp($request->query('sort'), "last_name") == 0]) }}> Name </a> 
          </th>
          <th>  
            <a href={{ route('admin.list', ['sort' => 'uwnetid', 'order' => $order, 'flip'=> strcmp($request->query('sort'), "uwnetid") == 0]) }}> NetID </a> 
          </th>
          <th> 
            <a href={{ route('admin.list', ['sort' => 'updated_at', 'order' => $order, 'flip'=> strcmp($request->query('sort'), "updated_at") == 0]) }}> Last Modified </a> 
          </th>
          <th> 
            <a href={{ route('admin.list', ['sort' => 'published_at', 'order' => $order, 'flip'=> strcmp($request->query('sort'), "published_at") == 0]) }}> Published </a> 
          </th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($people as $person)
          <tr>
            <td>
              <img src="{{ $person->photoUrl }}/40/40?c={{ $person->photo->id ?? 0 }}" alt="Profile Photo" class="thumb rounded-circle profile-picture">
            </td>
            <td>
              {{ $person->name }}
            </td>
            <td>
              {{ $person->uwnetid }}
            </td>
            <td>
              {{ $person->updated_at }}
            </td>
            <td>
              @if ($person->status == 'published')
                <span class="p-1 alert alert-success">Published</span>
              @elseif ($person->status == 'unpublished')
                <span class="p-1 alert alert-warning">Unpublished</span>
              @else
                <span class="p-1 alert alert-info">Revised</span>
              @endif
            </td>
            <td class="p-0">
              <div class="dropdown">
                <div id="person-dropdown-{{ $person->id }}" type="button" data-toggle="dropdown" aria-expanded="false" class="td-padding dropdown-toggle">
                  <i aria-hidden="true" class="fa fa-ellipsis-v fa-lg"></i>
                </div>
                <div aria-labelledby="person-dropdown-{{ $person->id }}" class="dropdown-menu dropdown-menu-right">
                  <a href="{{ route('user.appointment.list', ['person' => $person]) }}" class="dropdown-item">Edit</a>
                  <a href="{{ route('admin.person.revisions', $person) }}" class="dropdown-item">Revisions</a>
                  <a href="{{ route('admin.person.unpublish', $person) }}" class="dropdown-item">Unpublish</a>
                  <a href="{{ route('admin.user.publish', $person) }}" class="dropdown-item">Publish</a>
                  <a href="{{ route('admin.person.delete', $person) }}" class="dropdown-item">Delete</a>
                </div>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  {{ $people->appends(['search' => $search])->links('vendor.pagination.custom') }}

@endsection
