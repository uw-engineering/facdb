@extends('admin.dashboards.basic')

@section('scripts')
@parent
<script>
  $(function () {
    $('.add-search-person').on('click', function (e) {
      e.preventDefault();
      $('#uwregid').val($(this).attr('id'));
      $('#uwregid-form').submit();
    });
  });
</script>
@endsection

@section('main')

<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-block">
          
        <h2 class="card-title">Add a person</h2>
        <h6 class="card-subtitle text-muted">
          Enter the basic information for the new FacDB Faculty member.
        </h6>

        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <form id="uwregid-form" method="post">
          {{ csrf_field() }}

          <input type="hidden" id="uwregid" name="uwregid" value="">

          <div class="form-group row">
            <label for="first_name" class="col-sm-3 col-form-label">First Name</label>
            <div class="col-sm-9">
              <input type="text" 
                class="form-control" 
                id="first_name" 
                name="first_name" 
                placeholder="First Name"
                value="{{ old('first_name') }}"
              >
            </div>
          </div>
          <div class="form-group row">
            <label for="last_name" class="col-sm-3 col-form-label">Last Name</label>
            <div class="col-sm-9">
              <input type="text" 
                class="form-control" 
                id="last_name" 
                name="last_name" 
                placeholder="Last Name"
                value="{{ old('last_name') }}"
              >
            </div>
          </div>

          @if (isset($search))
            <p>
              Found {{ $search->TotalCount }} faculty member(s) matching that name.
            </p>
            <table class="table">
              @foreach ($search->Persons as $person)
                <tr>
                  <td>
                    {{ $person->DisplayName }}
                  </td>
                  <td>
                    {{ $person->PersonAffiliations->EmployeePersonAffiliation->HomeDepartment }}
                  </td>
                  <td>
                    <button class="btn btn-primary add-search-person" id="{{ $person->UWRegID }}" type="submit" class="btn btn-primary">Add</button>
                  </td>
                </tr>
              @endforeach
            </table>
          @endif

          <div class="form-group row">
            <div class="col">
              <div class="d-flex flex-row-reverse">
                <button type="submit" class="btn btn-primary">Search</button>
              </div>
            </div>
          </div>

        </form>

        <form id="uwnetid-form" method="post">
          {{ csrf_field() }}

          <p>
            -- OR --
          </p>

          <div class="form-group row">
            <label for="uwnetid" class="col-sm-3 col-form-label">UWNetID</label>
            <div class="col-sm-9">
              <input type="text" 
                class="form-control"
                name="uwnetid" 
                placeholder="uwnetid"
              >
            </div>
          </div>

          <div class="form-group row">
            <div class="col">
              <a href="{{ \App\Filament\Resources\PersonResource::getUrl('index') }}" class="btn btn-secondary">Cancel</a>
            </div>
            <div class="col">
              <div class="d-flex flex-row-reverse">
                <button type="submit" class="btn btn-primary">Add</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
