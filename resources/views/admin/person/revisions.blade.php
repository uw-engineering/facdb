@extends('admin.dashboards.basic')

@section('main')

<div class="row">
  <div class="col">

      <div class="card">
        <div class="card-block">
            
          <h2 class="card-title">Current Revisions for {{ $person->name }}</h2>

          <h3>
            Bio
          </h3>
          <table class="table">
            <thead>
              <tr>
                <th>Field</th>
                <th>Old Value</th>
                <th>New Value</th>
                <th>Modified</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($person->revisions as $revision)
              <tr>
                <td>{{ $revision->key }}</td>
                <td>{{ $revision->old_value }}</td>
                <td>{{ $revision->new_value }}</td>
                <td>{{ $revision->updated_at }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
          
          @foreach ($person->appointmentsWithTrashed as $appointment)
            @if ($appointment->revised)
              <h3>
                Appointment:
                {{ $appointment->department ? $appointment->department->name : $appointment->other_department }}
                {{ $appointment->title }}
              </h3>
              <table class="table">
                <thead>
                  <tr>
                    <th>Field</th>
                    <th>Old Value</th>
                    <th>New Value</th>
                    <th>Modified</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($appointment->revisions as $revision)
                  <tr>
                    <td>{{ $revision->key }}</td>
                    <td>{{ $revision->old_value }}</td>
                    <td>{{ $revision->new_value }}</td>
                    <td>{{ $revision->updated_at }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            @endif
          @endforeach

          @foreach ($person->listingsWithTrashed as $listingUsed)
            @if ($listingUsed->revised)
              <h3>
                Listing: {{ $listingUsed->department->name }}
              </h3>
              <table class="table">
                <thead>
                  <tr>
                    <th>Field</th>
                    <th>Old Value</th>
                    <th>New Value</th>
                    <th>Modified</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($listingUsed->revisions as $revision)
                  <tr>
                    <td>{{ $revision->key }}</td>
                    <td>{{ $revision->old_value }}</td>
                    <td>{{ $revision->new_value }}</td>
                    <td>{{ $revision->updated_at }}</td>
                  </tr>
                @endforeach
                </tbody>

                @foreach ($listingUsed->affiliationsWithTrashed as $affiliation)
                  @if ($affiliation->revised)
                    <thead>
                      <th colspan="4">Affiliation revised: {{ $affiliation->name }}</th>
                    </thead>
                    <tbody>
                      @foreach ($affiliation->revisions as $revision)
                        <tr>
                          <td>{{ $revision->key }}</td>
                          <td>{{ $revision->old_value }}</td>
                          <td>{{ $revision->new_value }}</td>
                          <td>{{ $revision->updated_at }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  @endif
                @endforeach

                @foreach ($listingUsed->projectsWithTrashed as $project)
                  @if ($project->revised)
                    <thead>
                      <th colspan="4">Project revised: {{ $project->name }}</th>
                    </thead>
                    <tbody>
                      @foreach ($project->revisions as $revision)
                        <tr>
                          <td>{{ $revision->key }}</td>
                          <td>{{ $revision->old_value }}</td>
                          <td>{{ $revision->new_value }}</td>
                          <td>{{ $revision->updated_at }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  @endif
                @endforeach

              </table>
            @endif
          @endforeach

        </div>
      </div>



    <div class="card mt-5">
      <div class="card-block">
          
        <h2 class="card-title">Prior Revisions</h2>
        <h6 class="card-subtitle text-muted">
          All tracked changes on record
        </h6>

        <h3>
          Bio
        </h3>
        <table class="table">
          <thead>
            <tr>
              <th>Field</th>
              <th>Old Value</th>
              <th>New Value</th>
              <th>Modified</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($person->oldRevisions as $revision)
              <tr>
                <td>{{ $revision->key }}</td>
                <td>{{ $revision->old_value }}</td>
                <td>{{ $revision->new_value }}</td>
                <td>{{ $revision->updated_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
        
        @foreach ($person->appointmentsWithTrashed as $appointment)
          <h3 class="mt-5">
            Appointment:
            {{ $appointment->department ? $appointment->department->name : $appointment->other_department }}
            {{ $appointment->title }}
          </h3>
          <table class="table">
            <thead>
              <tr>
                <th>Field</th>
                <th>Old Value</th>
                <th>New Value</th>
                <th>Modified</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($appointment->oldRevisions as $revision)
                <tr>
                  <td>{{ $revision->key }}</td>
                  <td>{{ $revision->old_value }}</td>
                  <td>{{ $revision->new_value }}</td>
                  <td>{{ $revision->updated_at }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        @endforeach

        @foreach ($person->listingsWithTrashed as $listingUsed)
          <h3 class="mt-5">
            Listing: {{ $listingUsed->department->name }}
          </h3>
          <table class="table">
            <thead>
              <tr>
                <th>Field</th>
                <th>Old Value</th>
                <th>New Value</th>
                <th>Modified</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($listingUsed->oldRevisions as $revision)
              <tr>
                <td>{{ $revision->key }}</td>
                <td>{{ $revision->old_value }}</td>
                <td>{{ $revision->new_value }}</td>
                <td>{{ $revision->updated_at }}</td>
              </tr>
            @endforeach
            </tbody>

            @foreach ($listingUsed->affiliationsWithTrashed as $affiliation)
              <thead>
                <th colspan="4">Affiliation revised: {{ $affiliation->name }}</th>
              </thead>
              <tbody>
                @foreach ($affiliation->oldRevisions as $revision)
                  <tr>
                    <td>{{ $revision->key }}</td>
                    <td>{{ $revision->old_value }}</td>
                    <td>{{ $revision->new_value }}</td>
                    <td>{{ $revision->updated_at }}</td>
                  </tr>
                @endforeach
              </tbody>
            @endforeach

            @foreach ($listingUsed->projectsWithTrashed as $project)
              <thead>
                <th colspan="4">Project revised: {{ $project->name }}</th>
              </thead>
              <tbody>
                @foreach ($project->oldRevisions as $revision)
                  <tr>
                    <td>{{ $revision->key }}</td>
                    <td>{{ $revision->old_value }}</td>
                    <td>{{ $revision->new_value }}</td>
                    <td>{{ $revision->updated_at }}</td>
                  </tr>
                @endforeach
              </tbody>
            @endforeach

          </table>
        @endforeach

      </div>
    </div>


  </div>
</div>

@endsection
