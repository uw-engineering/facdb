@extends('dashboard::page')
@section('title', 'Welcome')
@section('navbar-text', 'UW CoE Faculty Database')

@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
@endsection

@section('scripts')
  @parent
  <script src="{{ asset(mix('js/app.js')) }}"></script>
@endsection

@section('navbar')
<ul class="navbar-nav mr-auto">
  <li class="nav-item">
    <a class="nav-link" href="#">Faculty Database Admin Tools<span class="sr-only">(current)</span></a>
  </li>
</ul>
@endsection

@section('sidebar-left')
  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link" href="{{ \App\Filament\Resources\Person\Pages\ListPeople::getUrl() }}">
        <i class="fa fa-id-card fa-fw" aria-hidden="true"></i>&nbsp;
        Faculty List <span class="sr-only">(current)</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('admin.research.select') }}">
        <i class="fa fa-flask fa-fw" aria-hidden="true"></i>&nbsp;
        Research Areas <span class="sr-only">(current)</span>
      </a>
    </li>
  </ul>
@endsection
