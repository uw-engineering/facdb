@extends('user.dashboards.listing')
@section('title', 'Honors &amp; Awards')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Honors &amp; Awards</li>
@endsection

@section('main')

  <div class="row">
    <div class="col">
      <div class="card multi-step">
        <div class="card-header">
          <ul class="nav nav-pills card-header-pills">
            @php
              $routes = [
                'user.honors.awards' => 'Awards',
                'user.honors.fellowships' => 'Society fellowships',
                'user.honors.memberships' => 'Academy Memberships',
              ];
              $current = Route::currentRouteName();
            @endphp
            @foreach ($routes as $route => $name)
              @php
                $visited = array_search($route, array_keys($routes)) < array_search($current, array_keys($routes));
                $active = $current == $route;
              @endphp
              <li class="nav-item">
                @if ($active)
                  <span class="nav-link active">{{ $name }}</span>
                @else
                  <a class="nav-link {{ $visited ? 'visited' : '' }}" href="{{ route($route, $listing) }}">{{ $name }}</a>
                @endif
              </li>
            @endforeach
          </ul>
        </div>
        <div class="card-block">

          @yield('card')

        </div>
      </div>
    </div>
  </div>

@endsection
