@extends('user.honors.base')
@section('title', 'Honors &amp; Awards - Academy Memberships')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Academy Memberships</li>
@endsection

@section('card')

  <h2 class="card-title">Academy memberships</h2>
  <h6 class="card-subtitle text-muted">
  Arrange in the order you prefer by starting with the membership that should appear first.
  </h6>
  <ul>
  <li>To add another, click the plus sign</li> 
  <li>To remove, click the minus sign</li>
  </ul>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Academy &amp; year inducted" name="memberships" :list="$listing->memberships"/>

    <p><strong>You have reached the end! Click save and either review sections, create another listing for a UW Engineering department, or send email to <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> to request approval of your new faculty page.<strong></p>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.honors.fellowships', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>

  </form>

@endsection
