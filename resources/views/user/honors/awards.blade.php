@extends('user.honors.base')
@section('title', 'Honors &amp; Awards - Awards')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Awards</li>
@endsection

@section('card')

  <h2 class="card-title">Awards</h2>
  <h6 class="card-subtitle text-muted">
  Arrange in the order you prefer by starting with the award that should appear first.
  </h6>
  <ul>
  <li>10 award limit</li>
  <li>To add another, click the plus sign</li> 
  <li>To remove, click the minus sign</li>
  </ul>
  <p><strong>Note that you will be prompted to list society fellowships and academy memberships n subsequent pages.<strong></p>          @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Award name &amp; year" name="awards" :list="$listing->awards"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.work.students', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
