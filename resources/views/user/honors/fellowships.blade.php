@extends('user.honors.base')
@section('title', 'Honors &amp; Awards - Society Fellowships')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Society Fellowships</li>
@endsection

@section('card')

  <h2 class="card-title">Society fellowships</h2>
  <h6 class="card-subtitle text-muted">
  Arrange in the order you prefer by starting with the fellowship that should appear first.
  </h6>
  <ul>
  <li>To add another, click the plus sign</li> 
  <li>To remove, click the minus sign</li>
  </ul>
  <p><strong>Note that you will be prompted to list academy memberships on subsequent pages.<strong></p>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Society &amp; year inducted" name="fellowships" :list="$listing->fellowships"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.honors.awards', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
