@extends('user.work.base')
@section('title', 'Current Work - Courses Taught')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Courses Taught</li>
@endsection

@section('card')

  <h2 class="card-title">Courses Taught</h2>
  <h6 class="card-subtitle text-muted">
    List individually: to add another, click the plus sign.
  </h6>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Course number & name" name="courses" :list="$listing->courses"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.publication.list', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Next</button>
        </div>
      </div>
    </div>

  </form>

@endsection
