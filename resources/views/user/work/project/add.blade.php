@extends('user.work.base')
@section('title', 'Current Work - Current Projects')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Current Projects</li>
@endsection

@section('card')

  <h2 class="card-title">New Project</h2>
  <h6 class="card-subtitle text-muted">Please detail your project.</h6>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}

    <div class="form-group row">
      <label for="name" class="col-sm-3 col-form-label">Project Name</label>
      <div class="col-sm-9">
        <input type="text" 
          class="form-control" 
          id="name" 
          name="name" 
          placeholder="Name" 
          value="{{ old('name') }}"
        >
      </div>
    </div>

    <div id="summernote"></div>
    <input id="description" type="hidden" name="description" value="">

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.work.project.list', $listing) }}" class="btn btn-secondary">Cancel</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="save" type="submit" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  @parent
  <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
  <script>
    $(document).ready(function () {
      $('#summernote').summernote({
        height: 300,
        focus: true,
        codemirror: { // codemirror options
          theme: 'monokai'
        },
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['codeview', ['codeview']],
        ]
      });
      $('#save').on('click', function (e) {
        $('#description').val($('#summernote').summernote('code'));
      });
    });
  </script>
@endsection
