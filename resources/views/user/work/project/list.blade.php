@extends('user.work.base')
@section('title', 'Current Work - Current Projects')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Current Projects</li>
@endsection

@section('card')

  <h2 class="card-title">Current Projects</h2>
  <h6 class="card-subtitle text-muted">Each project you add has room for a project name and a project description that can be formatted.</h6>
  <ul>
  <li>Arrange in the order you prefer by starting with the project that should appear first.</li>
  <li>3 project limit</li>
  <li>To add another, click the plus sign</li>
  <li>To remove one, click the minus sign</li>
  </ul>
  <table class="table">
    <thead>
      <tr>
        <th>Project Name</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($listing->projects as $project)
        <tr>
          <td>{{ $project->name }}</td>
          <td>
            <a href="{{ route('user.work.project.edit', [$listing, $project]) }}">Edit</a>
            <a href="{{ route('user.work.project.delete', [$listing, $project]) }}">Remove</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <div class="row">
    <div class="col">
      <div class="d-flex flex-row-reverse">
        <a href="{{ route('user.work.project.add', $listing) }}" class="btn btn-primary">Add a project</a>
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col">
      <a href="{{ route('user.work.courses', $listing) }}" class="btn btn-secondary">Previous</a>
    </div>
    <div class="col">
      <div class="d-flex flex-row-reverse">
      <a href="{{ route('user.work.students', $listing) }}" class="btn btn-primary">Save and Continue</a>
      </div>
    </div>
  </div>


@endsection
