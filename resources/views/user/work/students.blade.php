@extends('user.work.base')
@section('title', 'Current Work - Ph.D. Students')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Ph.D. Students</li>
@endsection

@section('card')

  <h2 class="card-title">Ph.D. Students</h2>
  <h6 class="card-subtitle text-muted">
    List individually: to add another, click the plus sign.
  </h6>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Student name" name="students" :list="$listing->students"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.work.project.list', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Next</button>
        </div>
      </div>
    </div>

  </form>

@endsection
