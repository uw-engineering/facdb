@extends('user.history.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Biography</li>
@endsection

@section('head')
  @parent
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/summernote/summernote-bs4.css') }}">
@endsection

@section('card')

  <h2 class="card-title">Biography</h2>
  <h6 class="card-subtitle text-muted">General description of your work, as you would provide to a conference where you're presenting.
  </h6>
  <ul>
    <li>2000 character limit.</li>
    <li>Contact <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> for help with adding images, formatting, or editing.</li>
    </ul>
  <p><strong>Note that you will be prompted for a detailed research statement in the RESEARCH section and you can describe current work in detail in the CURRENT WORK section.</strong></p>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <div id="summernote"></div>
    <input id="bio" type="hidden" name="bio" value="">

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.history.appointments', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('scripts')
  @parent
  <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
  <script>
    $(document).ready(function () {
      $('#summernote').summernote({
        height: 300,
        focus: true,
        codemirror: { // codemirror options
          theme: 'monokai'
        },
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['codeview', ['codeview']],
        ]
      }).summernote('code', {!! json_encode($listing->bio) !!});
      $('#next').on('click', function (e) {
        $('#bio').val($('#summernote').summernote('code'));
      });
    });
  </script>
@endsection
