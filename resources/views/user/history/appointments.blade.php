@extends('user.history.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Previous Appointments</li>
@endsection

@section('card')

  <h2 class="card-title">Previous Appointments</h2>
  <h6 class="card-subtitle text-muted">List up to 3 appointments at other UW departments or other institutions, most recent first.</h6>
  <ul>
  <li>Click the plus sign to add a new line below.</li>
  <li>This section is optional. Lines and sections left blank will not appear on the published faculty page.</li>
  </ul>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Ph.D. or equivalent" name="prior_appointments" :list="$listing->prior_appointments"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.history.academics', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
