@extends('user.history.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Academics</li>
@endsection

@section('card')

  <h2 class="card-title">Academic History</h2>
  <h6 class="card-subtitle text-muted">
    List your degrees, one per line, most recent degree first.
  </h6>
  <ul>
    <li>
      To add another degree, click a plus sign; a new line will be added
      <em>below</em> the one you clicked.
    </li>
  </ul>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}

    <x-sortable label="Ph.D. or equivalent" name="doctorate" :list="$listing->doctorate"/>
    <x-sortable label="M.S. or equivalent" name="masters" :list="$listing->masters"/>
    <x-sortable label="B.S. or equivalent" name="bachelors" :list="$listing->bachelors"/>

    <div class="form-group row">
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">
            Save and Continue
          </button>
        </div>
      </div>
    </div>

  </form>

@endsection
