@extends('user.history.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Curriculum Vitae (C.V.)</li>
@endsection

@section('card')

  <h2 class="card-title">Curriculum Vitae (C.V.)</h2>
  <h6 class="card-subtitle text-muted">
    Upload the PDF version of your formal CV. This will appear as a link on your Faculty Finder profile.
  </h6>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group row">
      <label for="cv" class="col-sm-3 col-form-label">Choose file (PDF only)</label>
      <div class="col-sm-7">
        <div class="custom-file">
          <input name="cv" type="file" class="custom-file-input" id="cv">
          <label class="custom-file-label" for="cv">{{ $person->cv->name ?? 'Choose file' }}</label>
        </div>
      </div>
      @if ($person->cv)
        <div class="col-sm-2 text-right">
          <input type="submit" class="btn btn-danger" name="delete" value="delete">
        </div> 
      @endif
    </div>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.history.bio', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">
            Save and Continue
          </button>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('scripts')
  @parent
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
    })
  </script>
@endsection
