@extends('user.dashboards.listing')
@section('title', 'Labs &amp; Centers')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Labs &amp; Centers</li>
@endsection

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Edit Affiliation</h2>
          <h6 class="card-subtitle text-muted">Please detail your affiliation.</h6>

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post">
            {{ csrf_field() }}

            <div class="form-group row">
              <label for="email" class="col-sm-3 col-form-label">Type</label>
              <div class="col-sm-9">
                <select name="type" class="custom-select">
                  <option>Type</option>
                  <option {{ $affiliation->type == 'lab' ? 'selected' : '' }} value="lab">Lab</option>
                  <option {{ $affiliation->type == 'center' ? 'selected' : '' }} value="center">Center</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Name</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="name" 
                  name="name" 
                  placeholder="Name" 
                  value="{{ old('name', $affiliation->name) }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <label for="website" class="col-sm-3 col-form-label">Website</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="website" 
                  name="website" 
                  placeholder="Website" 
                  value="{{ old('website', $affiliation->website) }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <label for="phone" class="col-sm-3 col-form-label">Phone Number</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="phone" 
                  name="phone" 
                  placeholder="Phone Number" 
                  value="{{ old('phone', $affiliation->phone) }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <label for="location" class="col-sm-3 col-form-label">Location</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="location" 
                  name="location" 
                  placeholder="location" 
                  value="{{ old('location', $affiliation->location) }}"
                >
              </div>
            </div>

            <h3>Lab Research Areas</h3>
            @foreach ($areas as $area)
              <div class="custom-control custom-checkbox">
                <input type="checkbox" 
                  class="custom-control-input" 
                  name="areas[{{ $area->researchArea->id }}]"
                  id="research_area_id_{{ $area->researchArea->id }}"
                  @if ($affiliation->researchAreas->contains($area->researchArea))
                    checked="checked"
                  @endif
                >
                <label class="custom-control-label" for="research_area_id_{{ $area->researchArea->id }}">
                  {{ $area->researchArea->name }}
                </label>
              </div>
            @endforeach


            <div class="form-group row">
              <div class="col">
                <a href="{{ route('user.affiliation.list', $listing) }}" class="btn btn-secondary">Cancel</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
