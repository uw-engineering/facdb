@extends('user.dashboards.listing')
@section('title', 'Labs &amp; Centers')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Labs &amp; Centers</li>
@endsection

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Labs &amp; Centers</h2>
          <h6 class="card-subtitle text-muted">Enter your labs and centers</h6>

          <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Type</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($listing->affiliations as $affiliation)
                <tr>
                  <td>{{ $affiliation->name }}</td>
                  <td>{{ ucwords($affiliation->type) }}</td>
                  <td>
                    <a href="{{ route('user.affiliation.edit', [$listing, $affiliation]) }}">Edit</a>
                    <a href="{{ route('user.affiliation.delete', [$listing, $affiliation]) }}">Remove</a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          <div class="row">
            <div class="col">
              <div class="d-flex flex-row-reverse">
                <a href="{{ route('user.affiliation.add', $listing) }}" class="btn btn-primary">Add a lab or center</a>
              </div>
            </div>
          </div>

          <div class="form-group row">
              <div class="col">
                <a href="{{ route('user.research.statement', $listing) }}" class="btn btn-secondary">Previous</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                <a href="{{ route('user.publication.google', $listing) }}" class="btn btn-primary">Next</a>
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>

@endsection
