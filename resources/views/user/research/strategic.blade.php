@extends('user.research.base')
@section('title', 'Research - Strategic Research Areas')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Strategic Research Areas</li>
@endsection

@section('card')

  <h2 class="card-title">College of Engineering Strategic Research Areas</h2>
  <h6 class="card-subtitle text-muted">Select applicable research areas to highlight</h6>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}

    <div class="form-group">

      @foreach ($areas as $area)
        <div class="custom-control custom-checkbox">
          <input type="checkbox" 
            class="custom-control-input" 
            name="strategic[{{ $area->researchArea->id }}]"
            id="research_area_id_{{ $area->researchArea->id }}"
            @if ($listing->strategicResearchAreas->contains($area->researchArea))
              checked="checked"
            @endif
          >
          <label class="custom-control-label" for="research_area_id_{{ $area->researchArea->id }}">
            {{ $area->researchArea->name }}
          </label>
        </div>
      @endforeach

    </div>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.history.bio', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
