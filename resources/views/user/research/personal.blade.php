@extends('user.research.base')
@section('title', 'Research - Personal Research Areas')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Personal Research Areas</li>
@endsection

@section('card')

  <h2 class="card-title">Personal Research Areas</h2>
  <h6 class="card-subtitle text-muted">Add up to 5 research areas not covered under the College and Department research terms from the previous two pages.</p>
  </h6>
  <ul>
    <li>One research area per line</li>
    <li>To add another line, click the plus sign.</li>
    <li>75 character limit per line</li>
    <li>Up to 5 entries</li>
    <li>Sections and lines left blank will not appear on the published faculty page</p>
    </ul>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable label="Research Area" name="personal" :list="$listing->personalResearchAreas->pluck('name')"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.research.department', $listing->id) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
