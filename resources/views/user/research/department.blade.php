@extends('user.research.base')
@section('title', 'Research - Department Research Areas')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Department Research Areas</li>
@endsection

@section('card')

  <h2 class="card-title">Department Research Areas</h2>
  <h6 class="card-subtitle text-muted">Select applicable research areas to highlight</h6>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}

    @foreach ($areas as $area)
      <div class="row">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" 
            class="custom-control-input" 
            name="department[{{ $area->researchArea->id }}]"
            id="department_research_area_{{ $area->researchArea->id }}"
            @if ($listing->departmentResearchAreas->contains($area->researchArea))
              checked="checked"
            @endif
          >
          <label class="custom-control-label" for="department_research_area_{{ $area->researchArea->id }}">{{ $area->researchArea->name }}</label>
        </div>
      </div>
    @endforeach

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.research.strategic', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
