@extends('user.research.base')
@section('title', 'Research - Research Statement')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Research Statement</li>
@endsection

@section('card')

  <h2 class="card-title">Research Statement</h2>
  <h6 class="card-subtitle text-muted">General description of your research goals, successes, approach, etc. in paragraph form. </h6>
  <ul>
    <li>2000 character limit</li>
    <li>Basic formatting allowed. Contact <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> for help with adding images, formatting, or editing.</li>
    </ul>
    <p><strong>Note that you will be prompted for a detailed research statement in the RESEARCH section and you can describe current work in detail in the CURRENT WORK section.
</strong></p>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <div id="summernote"></div>
    <input id="research_statement" type="hidden" name="research_statement" value="">

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.research.personal', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('scripts')
  @parent
  <script>
    $(document).ready(function () {
      $('#summernote').summernote({
        height: 300,
        focus: true,
        codemirror: { // codemirror options
          theme: 'monokai'
        },
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['codeview', ['codeview']],
        ]
      }).summernote('code', {!! json_encode($listing->research_statement) !!});
      $('#next').on('click', function (e) {
        $('#research_statement').val($('#summernote').summernote('code'));
      });
    });
  </script>
@endsection
