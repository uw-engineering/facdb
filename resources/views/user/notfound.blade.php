@extends('user.dashboards.welcome')
@section('title', 'Welcome')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <!-- <div class="card-header">
          <h2>College of Engineering Faculty Thingy</h2>
        </div> -->
        <img class="card-img-top" src="https://www.washington.edu/wp-content/themes/uw-2014/assets/images/footer.jpg" alt="Card image cap">
        <div class="card-img-overlay card-inverse">
          <h2 class="card-title">College of Engineering Faculty Database</h2>
          <p class="card-text">A problem has occurred.</p>
          <!-- <p class="card-text"><small class="text-muted">Not to worry</small></p> -->
        </div>
        <div class="card-block">
          <h3>We were unable to locate a faculty record for you</h3>
          <p>
            Your faculty profile has not yet been created. Please email <a href="mailto:webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> and we'll get that fixed.
          </p>
        </div>
      </div>
    </div>
  </div>

@endsection
