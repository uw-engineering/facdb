@extends('user.dashboards.welcome')
@section('title', 'Welcome')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <!-- <div class="card-header">
          <h2>College of Engineering Faculty Thingy</h2>
        </div> -->
        <img class="card-img-top" src="https://www.washington.edu/wp-content/themes/uw-2014/assets/images/footer.jpg" alt="Card image cap">
        <div class="card-img-overlay card-inverse">
          <h2 class="card-title">College of Engineering Faculty Database</h2>
          <p class="card-text">Please take a moment to read the following information before you begin</p>
          <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
        </div>
        <div class="card-block">
          <h3>What is this information used for?</h3>
          <p>
            This form collects information for display on the College
            of Engineering Faculty Finder and participating UW Engineering department websites 
            (A&amp;A, CEE, ISE, ME, MSE and others in the future). You follow prompts to provide as much information as you want (within some limits).
          </p>
          <h3>When will my changes take effect?</h3>
          <p>
            For your changes to
            be published on the websites, notify <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> that your profile needs to be published. 
            The web team will review the content before publishing the changes. The
            review time varies based on the number of changes made. We may need to contact
            you prior to publishing the changes.
          </p>
          <h3>What will my profile look like?</h3>
          <p>
            If you belong to a department supported by the dean's office
            web team (A&amp;A, CEE, ISE, ME, and MSE), your faculty proﬁle
            will follow a stardard layout on your department website. View
            a complete <a href="https://www.mse.washington.edu/facultyfinder/xiaodong-xu">sample proﬁle</a>. 
            A subset of information is displayed on the College of Engineering 
            <a href="https://www.engr.washington.edu/facultyfinder">Find a Faculty Member page</a>.
          </p>
          <h3>How can I get help?</h3>
          <p>
            Contact <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> if you have questions or need help
            filling out the form.
          </p>
          <h3>What if I don’t want to display my information?</h3>
          <p>
            If you have a security or privacy concern about the public display
            of your faculty proﬁle, please contact <a href="webhelp@engr.uw.edu">webhelp@engr.uw.edu</a>. We will
            work with you to ensure that the information you want removed is
            taken oﬀ both your faculty proﬁle as well as any other locations on
            the websites we manage.
          </p>
          <div class="d-flex justify-content-center">
            <a href="{{ route('user.basic.name', $person) }}" class="btn btn-primary">Get Started</a>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
