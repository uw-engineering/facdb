@extends('dashboard::page')
@section('title', 'Welcome')
@section('navbar-text', 'UW CoE Faculty Database')


@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/codemirror/codemirror.css') }}">
@endsection

@section('scripts')
  @parent
  <script src="{{ asset(mix('js/app.js')) }}"></script>
  <script src="{{ asset('vendor/codemirror/codemirror.js') }}"></script>
  <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Listings</li>
  <li class="breadcrumb-item">{{ $listing->department->name }}</li>
@endsection

@section('navbar')
  <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="#">Faculty Finder Database<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      @if ($person->otherListings($listing)->count() || $person->canCreateListing())
        <div class="dropdown navbar-text d-flex align-items-center" 
          id="dropdown-listing" 
          data-toggle="dropdown" 
          aria-haspopup="true" 
          aria-expanded="false"
        >
          <span class="nav-link" href="#">
            {{ $listing->department->name }}
            <i class="fa fa-caret-down fa-fw" aria-hidden="true" style="font-size: 0.9em;"></i>
          </span>
        </div>
        
        <div class="dropdown-menu" aria-labelledby="dropdown-listing">
          @foreach ($person->otherListings($listing) as $other)
            <a class="dropdown-item" href="{{ route('user.history.academics', $other->id) }}">{{ $other->department->name }}</a>
          @endforeach
          @if ($person->canCreateListing())
            <a class="dropdown-item" href="{{ route('user.listing.add') }}">New Listing</a>
          @endif
        </div>
      @else
        <span class="nav-link" href="#">
          {{ $listing->department->name }}
        </span>
      @endif
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item active">
      <a class="nav-link" href="{{ route('user.listing.add') }}">
        <i class="fa fa-plus fa-fw" aria-hidden="true" style="font-size: 0.9em;"></i>
        New Listing <span class="sr-only">(current)</span>
      </a>
    </li>
  </ul>
@endsection

@section('alert')
  @if (Auth::user()->isAdmin())
    <div class="alert alert-warning">
      You are currently editing a faculty profile for {{ $person->name }}. You may return to the <a href="{{ \App\Filament\Resources\PersonResource::getUrl('index') }}">admin page</a>.
    </div>
  @endif

  @if ($person->revised)
    <x-revised-alert />
  @endif
@endsection

@section('sidebar-left')
  @php
    $route = Route::currentRouteName();
    $welcomeActive = $route == 'user.welcome';
    $appointmentsActive = \Illuminate\Support\Str::startsWith($route, 'user.appointment');
    $listingsActive = \Illuminate\Support\Str::startsWith($route, 'user.listing');
    $historyActive = \Illuminate\Support\Str::startsWith($route, 'user.history');
    $researchActive = \Illuminate\Support\Str::startsWith($route, 'user.research');
    $affiliationActive = \Illuminate\Support\Str::startsWith($route, 'user.affiliation');
    $publicationActive = \Illuminate\Support\Str::startsWith($route, 'user.publication');
    $workActive = \Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user.work');
    $honorsActive = \Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user.honors');
  @endphp
  <ul class="nav nav-pills flex-column">

    <li class="nav-item {{ $welcomeActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.basic.name') }}">
        <i class="fa fa-id-card fa-fw" aria-hidden="true"></i>&nbsp;
        Basic Information <span class="sr-only">(current)</span>
      </a>
    </li>

    <li class="nav-item {{ $appointmentsActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.appointment.list') }}">
        <i class="fa fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;
        Appointments
      </a>
    </li>

    <li class="nav-item {{ $listingsActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.listing.list') }}">
        <i class="fa fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;
        Listings
      </a>
    </li>

    <li class="nav-item" style="margin-left: 0px; margin-top: 20px;">
      <a class="nav-link" href="{{ route('user.listing.edit', $listing) }}">
        {{ $listing->department->name }}
      </a>
    </li>
    
    <li class="nav-item {{ $historyActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.history.academics', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-history fa-fw" aria-hidden="true"></i>&nbsp;
          History
        </span>
      </a>
      @if ($historyActive)
        <ul>
          <li>
            <a class="nav-link {{ $route == 'user.history.academics' ? 'active' : '' }}" href="{{ route('user.history.academics', $listing) }}">Academics</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.history.appointments' ? 'active' : '' }}" href="{{ route('user.history.appointments', $listing) }}">Previous Appointments</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.history.bio' ? 'active' : '' }}" href="{{ route('user.history.bio', $listing) }}">Biography</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.history.cv' ? 'active' : '' }}" href="{{ route('user.history.cv', $listing) }}">Curriculum Vitae (C.V.)</a>
          </li>
        </ul>
      @endif
    </li>

    <li class="nav-item {{ $researchActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.research.strategic', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-edit fa-fw" aria-hidden="true"></i>&nbsp;
          Research
        </span>
      </a>
      @if ($researchActive)
        <ul>
          <li>
            <a class="nav-link {{ $route == 'user.research.strategic' ? 'active' : '' }}" href="{{ route('user.research.strategic', $listing) }}">Strategic</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.research.department' ? 'active' : '' }}" href="{{ route('user.research.department', $listing) }}">Department</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.research.personal' ? 'active' : '' }}" href="{{ route('user.research.personal', $listing) }}">Personal</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.research.statement' ? 'active' : '' }}" href="{{ route('user.research.statement', $listing) }}">Statement</a>
          </li>
        </ul>
      @endif
    </li>

    <li class="nav-item {{ $affiliationActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.affiliation.list', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-flask fa-fw" aria-hidden="true"></i>&nbsp;
          Labs &amp; Centers
        </span>
      </a>
      @if ($affiliationActive)
        <ul>
          <li>
            <a class="nav-link active" href="{{ route('user.affiliation.list', $listing) }}">List</a>
          </li>
        </ul>
      @endif
    </li>

    <li class="nav-item {{ $publicationActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.publication.google', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-book fa-fw" aria-hidden="true"></i>&nbsp;
          Publications
        </span>
      </a>
      @if ($publicationActive)
        <ul>
          <li>
            <a class="nav-link {{ $route == 'user.publication.google' ? 'active' : '' }}" href="{{ route('user.publication.google', $listing) }}">Google Scholar</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.publication.list' ? 'active' : '' }}" href="{{ route('user.publication.list', $listing) }}">Select Publications</a>
          </li>
        </ul>
      @endif
    </li>

    <li class="nav-item {{ $workActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.work.courses', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-briefcase fa-fw" aria-hidden="true"></i>&nbsp;
          Current Work
        </span>
      </a>
      @if ($workActive)
        <ul>
          <li>
            <a class="nav-link {{ $route == 'user.work.courses' ? 'active' : '' }}" href="{{ route('user.work.courses', $listing) }}">Courses Taught</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.work.project.list' ? 'active' : '' }}" href="{{ route('user.work.project.list', $listing) }}">Current Projects</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.work.students' ? 'active' : '' }}" href="{{ route('user.work.students', $listing) }}">Ph.D. Students</a>
          </li>
        </ul>
      @endif
    </li>

    <li class="nav-item {{ $honorsActive ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.honors.awards', $listing) }}">
        <span style="padding-left: 20px;">
          <i class="fa fa-trophy fa-fw" aria-hidden="true"></i>&nbsp;
          Honors &amp; Awards
        </span>
      </a>
      @if ($honorsActive)
        <ul>
          <li>
            <a class="nav-link {{ $route == 'user.honors.awards' ? 'active' : '' }}" href="{{ route('user.honors.awards', $listing) }}">Awards</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.honors.fellowships' ? 'active' : '' }}" href="{{ route('user.honors.fellowships', $listing) }}">Society fellowships</a>
          </li>
          <li>
            <a class="nav-link {{ $route == 'user.honors.memberships' ? 'active' : '' }}" href="{{ route('user.honors.memberships', $listing) }}">Academy Memberships</a>
          </li>
        </ul>
      @endif
    </li>

  </ul>
@endsection
