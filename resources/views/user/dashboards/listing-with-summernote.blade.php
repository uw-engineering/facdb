@extends('user.dashboards.listing')

@section('head')
  @parent
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/codemirror/codemirror.css') }}">
@endsection


@section('scripts')
  @parent
  <script src="{{ asset('vendor/codemirror/codemirror.js') }}"></script>
  <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
@endsection
