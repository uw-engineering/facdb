@extends('dashboard::page')
@section('title', 'Welcome')
@section('navbar-text', 'UW CoE Faculty Database')

@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
@endsection

@section('scripts')
  @parent
  <script src="{{ asset(mix('js/app.js')) }}"></script>
@endsection

@section('navbar')
  <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="#">Faculty Finder Database<span class="sr-only">(current)</span></a>
    </li>
  </ul>
  @if ($person->canCreateListing())
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('user.listing.add') }}">
          <i class="fa fa-plus fa-fw" aria-hidden="true" style="font-size: 0.9em;"></i>
          New Listing <span class="sr-only">(current)</span>
        </a>
      </li>
    </ul>
  @endif
@endsection

@section('alert')
  @if (Auth::user()->isAdmin())
    <div class="alert alert-warning">
      You are currently editing a faculty profile for {{ $person->name }}. You may return to the <a href="{{ \App\Filament\Resources\PersonResource::getUrl('index') }}">admin page</a>.
    </div>
  @endif

  @if ($person->revised)
    <x-revised-alert />
  @endif
@endsection

@section('sidebar-left')
  <ul class="nav nav-pills flex-column">
    <li class="nav-item {{ \Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user.basic') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.basic.name') }}">
        <i class="fa fa-id-card fa-fw" aria-hidden="true"></i>&nbsp;
        Basic Information <span class="sr-only">(current)</span>
      </a>
    </li>
    <li class="nav-item {{ \Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user.appointment') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.appointment.list') }}">
        <i class="fa fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;
        Appointments
      </a>
    </li>
    <li class="nav-item {{ \Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user.listing') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('user.listing.list') }}">
        <i class="fa fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;
        Listings
      </a>
    </li>
    @foreach ($person->listings as $showListing)
      <li class="nav-item">
        <a class="nav-link" href="{{ route('user.history.academics', $showListing->id) }}"
          style="padding-left: 40px;">
          &bull;
          {{ $showListing->department->abbreviation }}
        </a>
      </li>
    @endforeach
  </ul>
@endsection
