@extends('dashboard::page')
@section('title', 'Welcome')
@section('navbar-text', 'UW CoE Faculty Database')

@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
@endsection

@section('sidebar')
  <ul class="nav nav-pills flex-column">
    <li class="nav-item active">
      <a class="nav-link active" href="#">
        <i class="fa fa-id-card-o fa-fw" aria-hidden="true"></i>&nbsp;
        Get Started <span class="sr-only">(current)</span>
      </a>
    </li>
  </ul>
@endsection
