@extends('user.dashboards.basic')
@section('title', 'Listing')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Add a Listing</h2>
          <h6 class="card-subtitle text-muted">Select a department to list. </h6>

          <form method="post">
            {{ csrf_field() }}

            <div class="form-group row">
              <label for="department" class="col-sm-3 col-form-label">Department</label>
              <div class="col-sm-9">
                <select name="department" class="custom-select">
                  <option selected>Department</option>
                  @foreach ($person->departmentsAvailableToList() as $department)
                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
            <p>If you have a separate, external site, list it here. To use your Faculty Finder faculty page, leave blank.</p>
              <label for="department_url" class="col-sm-3 col-form-label">URL for alternate faculty page or website</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="department_url" 
                  name="department_url" 
                  placeholder=""
                  value="{{ old('department_url') }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <div class="col">
                <a href="{{ route('user.listing.list') }}" class="btn btn-secondary">Cancel</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>

@endsection
