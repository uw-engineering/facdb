@extends('user.dashboards.basic')
@section('title', 'Listing')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Your Listings</h2>
          <p>Each UW Engineering department listing you created is now listed on the left side of the screen. You can now:
          <ul>
            <li>Edit your appointment info: Click the Edit button next to the department.</li>
            <li>Add another appointment: Click the Add a Listing button.</li>
            <li>Enter your professional info for a department listing: Click the department on the left side of the screen.</li> 
          </ul>

          @if (!$person->listings->count())
            @if ($person->canCreateListing())
              <div class="row">
                <div class="col">
                  <p>
                    Click the button below to create a new Faculty Finder listing.
                  </p>
                  <div class="d-flex justify-content-center">
                    <a href="{{ route('user.listing.add') }}" class="btn btn-primary">Add a Faculty Finder listing</a>
                  </div>
                </div>
              </div>
            @else
              <div class="row">
                <div class="col">
                  <p>
                    Create a departmental appointment by clicking Appointments on the left side of the screen. Then you can create a department listing.
                  </p>
                  <div class="d-flex justify-content-center">
                    <a href="{{ route('user.appointment.add') }}" class="btn btn-primary">Add a College Appointment</a>
                  </div>
                </div>
              </div>
            @endif
          @else
            <table class="table">
              <thead>
                <tr>
                  <th>Department</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($person->listings as $listingUsed)
                  <tr>
                    <td>{{ $listingUsed->department->name }}</td>
                    <td>
                        <a href="{{ route('user.listing.edit', $listingUsed) }}">Edit</a>
                    </td>
                    <td>
                      <a href="{{ route('user.listing.delete', $listingUsed) }}">Delete</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  @if ($person->canCreateListing())
                    <a href="{{ route('user.listing.add') }}" class="btn btn-primary">Add a listing</a>
                  @else
                    <a href="{{ route('user.listing.add') }}" class="btn btn-primary disabled">Add a listing</a>
                    <span class="small">You have no more available department listings</span>
                  @endif
                </div>
              </div>
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>

@endsection
