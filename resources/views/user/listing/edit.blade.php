@extends('user.dashboards.listing')
@section('title', 'Listing - Edit')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Edit Listing</h2>
          <h6 class="card-subtitle text-muted">Optional: Include the url to an alternate, up-to-date, maintained faculty page. </h6>

          <h6 class="card-subtitle text-muted"><i>Webmasters: Use this field for the old faculty page until the department is using the Faculty Finder faculty pages.</i></h6>

          <form method="post">
            {{ csrf_field() }}

            <div class="form-group row">
              <label for="department_url" class="col-sm-3 col-form-label">URL for alternate faculty page</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="department_url" 
                  name="department_url" 
                  placeholder=""
                  value="{{ old('department_url', $listing->department_url) }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <div class="col">
                <a href="{{ route('user.listing.list') }}" class="btn btn-secondary">Cancel</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>

@endsection
