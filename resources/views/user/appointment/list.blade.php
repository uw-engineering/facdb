@extends('user.dashboards.basic')
@section('title', 'Appointment')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Your Appointments</h2>
          <h6 class="card-subtitle text-muted">First define your appointments, then create one or more departmental listings.</h6>
          <ul>
            <li>Add a <i>UW Engineering department</i> appointment: Click Add an Appointment. </li>
            <li>Create a departmental listing for your first appointment: Click Add Your Professional Info. </li>
            <li>Create a different faculty page for each appointment: Click Add Another Appointment.  
            </ul>
          @if ($person->appointments->count())
            <table class="table">
              <thead>
                <tr>
                  <th>Department</th>
                  <th>Title</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($person->appointments as $appointment)
                  <tr>
                    <td>{{ $appointment->department ? $appointment->department->name : $appointment->other_department }}</td>
                    <td>{{ $appointment->title }}</td>
                    <td>
                      <a href="{{ route('user.appointment.edit', $appointment) }}">Edit</a>
                    </td>
                    <td>
                      <a href="{{ route('user.appointment.delete', $appointment) }}">Delete</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          @else
            Please enter information about your current faculty appointments.
          @endif

          <div class="row">
            <div class="col">
              <div class="d-flex flex-row-reverse">
                @if ($person->appointments->count())
                  @if ($person->canCreateListing())
                    <a href="{{ route('user.listing.add') }}" class="btn btn-primary">Add your professional info</a>
                    &nbsp;&nbsp;
                  @endif
                  <a href="{{ route('user.appointment.add') }}" class="btn btn-primary">Add another appointment</a>
                @else
                  <a href="{{ route('user.appointment.add') }}" class="btn btn-primary">Add an appointment</a>
                @endif
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

@endsection
