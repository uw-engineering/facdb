@extends('user.dashboards.basic')
@section('title', 'Appointment')

@section('main')

  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">

          <h2 class="card-title">Edit an Appointment</h2>
          <h6 class="card-subtitle text-muted">Enter your departmental information</h6>

          <form method="post">

            {{ csrf_field() }}

            <div class="form-group row">
              <label for="department" class="col-sm-3 col-form-label">Department</label>
              <div class="col-sm-9">
                <select name="department" class="custom-select">
                  <option value selected>Department</option>
                  @foreach ($departments as $department)
                    <option value="{{ $department->id }}"
                      {{ $appointment->department && $appointment->department->id == $department->id ? 'selected' : '' }}
                    >{{ $department->name }}</option>
                  @endforeach
                  <option value>Other</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="other_department" class="col-sm-3 col-form-label">Non-Engineering Department</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="other_department" 
                  name="other_department" 
                  placeholder=""
                  value="{{ $appointment->other_department }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <label for="title" class="col-sm-3 col-form-label">Title</label>
              <div class="col-sm-5">
                <select name="title" class="custom-select">
                  <option selected>Title</option>
                  @foreach (config('positions.titles') as $title)
                    <option value="{{ $title }}"
                      {{ $appointment->title == $title ? 'selected' : '' }}
                    >{{ $title }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-2">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" 
                    class="custom-control-input" 
                    name="adjunct"
                    id="adjunct"
                    {{ $appointment->adjunct ? 'checked' : '' }}
                  >
                  <label class="custom-control-label" for="adjunct">Adjunct</label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="affiliate" name="affiliate" {{ $appointment->affiliate ? 'checked' : '' }}>
                  <label class="custom-control-label" for="affiliate">Affiliate</label>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label for="endowed_title" class="col-sm-3 col-form-label">Endowment Title</label>
              <div class="col-sm-9">
                <input type="text" 
                  class="form-control" 
                  id="endowed_title" 
                  name="endowed_title" 
                  placeholder=""
                  value="{{ $appointment->endowed_title }}"
                >
              </div>
            </div>

            <div class="form-group row">
              <div class="col">
                <a href="{{ route('user.appointment.list') }}" class="btn btn-secondary">Cancel</a>
              </div>
              <div class="col">
                <div class="d-flex flex-row-reverse">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>

@endsection
