@extends('user.basic.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Photo</li>
@endsection

@section('scripts')
  @parent
  <script>
  function previewPhoto() {
    //var preview = document.querySelector('img');
    //var file    = document.querySelector('input[type=file]').files[0];
    //var reader  = new FileReader();

    var preview = $('#photo-file-preview');
    var file = $('#photo-file-input')[0].files[0]
    var title = $('#photo-file-title');
    var reader = new FileReader();

    reader.onloadend = function () {
      preview.attr('src', reader.result);
    }
  
    if (file) {
      reader.readAsDataURL(file);
      title.attr('data-content', file.name);
    } else {
      // preview.attr('src', '{{ asset('images/photo-default.png') }}');
      // title.attr('data-content', 'Choose file...');
    }
  }
  </script>
@endsection

@section('base-main')

  <div class="card-block">
    <form enctype="multipart/form-data" method="post">
      {{ csrf_field() }}

      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      <div class="form-group row">
        <div class="col">
          <img id="photo-file-preview" 
            class="img-thumbnail"
            alt="default photo"
            src="{{ $photoUrl }}"
            style="height: 238px; width: 100%;"
          >
        </div>
        <div class="col-md-8">
          <p>
            Provide the largest photo you have. 
            <ul>
              <li>JPEG 2000, JPEG or PNG format</li>
              <li>Minimum dimensions: 246x246 pixels</li>
              <li>Maximum filesize: 5Mb</li>
            </ul>
            <strong>Note:</strong> Contact <a href="mailto:webhelp@engr.uw.edu">webhelp@engr.uw.edu</a> if you have questions about providing a photo or if you need help cropping.
          </p>
          <label class="custom-file" style="width: 500px;">
            <input id="photo-file-input" name="photo" type="file" class="custom-file-input" accepts="image/*" onchange="previewPhoto()">
            <span id="photo-file-title"
              class="custom-file-control custom-file-name"
              data-content="{{ $photoTitle }}"></span>
          </label>
        </div>
      </div>

      <div class="form-group row">
        <div class="col">
          <a href="{{ route('user.basic.name') }}" class="btn btn-secondary">Previous</a>
        </div>
        <div class="col">
          <div class="d-flex flex-row-reverse">
            <button type="submit" class="btn btn-primary">Save and Continue</button>
          </div>
        </div>
      </div>
    </form>
  </div>

@endsection
