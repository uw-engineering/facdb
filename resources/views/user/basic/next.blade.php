@extends('user.basic.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Next Steps</li>
@endsection

@section('base-main')

  <div class="card-block">

    <p>
    For each tenure-level departmental faculty appointment you can create a separate faculty page that will appear on the department's website. </p>
    <p>For example, if you are a Professor in ISE and also an Adjunct Professor in ME, you can have ISE-specific info on the ISE website and ME-specific info on the ME site. On the College faculty directory, links to both pages are shown. For example, see <a href="https://www.engr.washington.edu/facultyfinder?search=ashis&dept=&area=">Ashis Banjaree</a> on the College website.
    </p>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.basic.admin') }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <a href="{{ route('user.appointment.add') }}" class="btn btn-primary">Add an Appointment</a>
        </div>
      </div>
    </div>

  </div>

@endsection
