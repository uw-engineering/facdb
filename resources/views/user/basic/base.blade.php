@extends('user.dashboards.basic')
@section('title', 'Welcome')

@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
@endsection

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Basic Information</li>
@endsection

@section('main')

  <div class="row">
    <div class="col">
      <div class="card multi-step">
        <div class="card-header">
          <ul class="nav nav-pills card-header-pills">
            @php
              $routes = [
                'user.basic.name' => '1. Name',
                'user.basic.photo' => '2. Photo',
                'user.basic.contact' => '3. Contact',
                'user.basic.admin' => '4. Administrative',
                'user.basic.next' => '5. Next Steps',
              ];
              $current = Route::currentRouteName();
            @endphp
            @foreach ($routes as $route => $name)
              @php
                $visited = array_search($route, array_keys($routes)) < array_search($current, array_keys($routes));
                $active = $current == $route;
              @endphp
              <li class="nav-item">
                @if ($active)
                  <span class="nav-link active">{{ $name }}</span>
                @else
                  <a class="nav-link {{ $visited ? 'visited' : '' }}" href="{{ route($route) }}">{{ $name }}</a>
                @endif
              </li>
            @endforeach
          </ul>
        </div>
        @yield('base-main')
      </div>
    </div>
  </div>

@endsection
