@extends('user.basic.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Name</li>
@endsection

@section('base-main')

  <div class="card-block">

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form method="post">
      {{ csrf_field() }}
      <div class="form-group row">
        <label for="first_name" class="col-sm-3 col-form-label">First Name</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="first_name" 
            name="first_name" 
            placeholder="First Name" 
            value="{{ old('first_name', $person->first_name) }}"
          >
        </div>
      </div>
      <div class="form-group row">
        <label for="middle_name" class="col-sm-3 col-form-label">Middle Name</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="middle_name" 
            name="middle_name" 
            placeholder="Middle Name"
            value="{{ old('middle_name', $person->middle_name) }}"
          >
        </div>
      </div>
      <div class="form-group row">
        <label for="last_name" class="col-sm-3 col-form-label">Last Name</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="last_name" 
            name="last_name" 
            placeholder="Last Name"
            value="{{ old('last_name', $person->last_name) }}"
          >
        </div>
      </div>
      <div class="form-group row">
        <label for="pronouns" class="col-sm-3 col-form-label">Pronouns</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="pronouns" 
            name="pronouns" 
            placeholder="Pronouns" 
            value="{{ old('pronouns', $person->pronouns) }}"
          >
        </div>
      </div>
      <div class="form-group row">
        <!-- <div class="col">
          <a href="{{ route('user.welcome') }}" class="btn btn-secondary">Previous</a>
        </div> -->
        <div class="col">
          <div class="d-flex flex-row-reverse">
            <button type="submit" class="btn btn-primary">Save and Continue</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  
@endsection
