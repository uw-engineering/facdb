@extends('user.basic.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Contact</li>
@endsection

@section('base-main')

  <div class="card-block">

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form method="post">
      {{ csrf_field() }}

      <p>Enter lab contact information, including lab websites, separately.</p>

      <div class="form-group row">
        <label for="email" class="col-sm-3 col-form-label">Email Address</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="email" 
            name="email" 
            placeholder="Email Address" 
            value="{{ old('email', $person->email) }}"
          >
        </div>
      </div>

      <div class="form-group row">
        <label for="office_number" class="col-sm-3 col-form-label">Office</label>
        <div class="col-sm-6">
          <select name="office_building" class="custom-select">
              <option value="" {{ !old('office_building', $person->office_building) ? 'selected' : '' }}>Office Building</option>
              @foreach ($buildings as $building)
                <option
                  {{ old('office_building', $person->office_building) == $building->code ? 'selected' : '' }}
                  value="{{ $building->code }}"
                >
                  {{ $building->code}} - {{ $building->name }}
                </option>
              @endforeach
            </select>
        </div>
        <div class="col-sm-2">
          <input type="text" 
            class="form-control" 
            id="office_number" 
            name="office_number" 
            placeholder="Office Number" 
            value="{{ old('office_number', $person->office_number) }}"
          >
        </div>
      </div>

      <div class="form-group row">
        <label for="phone" class="col-sm-3 col-form-label">Phone Number</label>
        <div class="col-sm-9">
          <input type="text" 
            class="form-control" 
            id="phone" 
            name="phone" 
            placeholder="(___) ___ - ____" 
            value="{{ old('phone', $person->phone) }}"
          >
        </div>
      </div>

      <div class="form-group row">
        <label for="website_url" class="col-sm-3 col-form-label">Faculty website</label>
        <div class="col-sm-9">
          <input type="text"
            class="form-control"
            id="website_url"
            name="website_url"
            placeholder="Faculty Website" 
            value="{{ old('website_url', $person->website_url) }}"
          >
        </div>
      </div>

      <x-sortable label="Social Media Links" name="social" :list="$person->social"/>
      
      <div class="form-group row">
        <span class="col-sm-3 col-form-label"></span>
        <p class="small gray col-sm-9"><i>You may add up to four links to social media channels including Twitter, Youtube, LinkedIn, Instagram, Github and Facebook.</i></p>
      </div>
      
      <div class="form-group row">
        <div class="col">
          <a href="{{ route('user.basic.photo') }}" class="btn btn-secondary">Previous</a>
        </div>
        <div class="col">
          <div class="d-flex flex-row-reverse">
            <button type="submit" class="btn btn-primary">Save and Continue</button>
          </div>
        </div>
      </div>
    </form>
  </div>

@endsection
