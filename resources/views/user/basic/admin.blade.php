@extends('user.basic.base')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item active" aria-current="page">Administrative</li>
@endsection

@section('base-main')

  <div class="card-block">

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form method="post">
      {{ csrf_field() }}
      <p>
      Administrative titles, such as Associate Dean of XYZ or Chair of ABC
      </p>

      <x-sortable label="Administrative Titles" name="title" :list="$person->title"/>

      <div class="form-group row">
        <div class="col">
          <a href="{{ route('user.basic.contact') }}" class="btn btn-secondary">Previous</a>
        </div>
        <div class="col">
          <div class="d-flex flex-row-reverse">
            <button type="submit" class="btn btn-primary">Save and Continue</button>
          </div>
        </div>
      </div>
    </form>
  </div>

@endsection
