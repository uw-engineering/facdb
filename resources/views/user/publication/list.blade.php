@extends('user.publication.base')
@section('title', 'Publications - Select Publications')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Select Publications</li>
@endsection

@section('card')

  <h2 class="card-title">Select Publications</h2>
  <h6 class="card-subtitle text-muted">
    List your most recent or relevant publications.
  </h6>
  <ul>
  <li>20 publications maximum; one publication per line</li>
  <li>Most recent publications first</li>
  <li>To add another, click the plus sign</li> 
  <li>To remove, click the minus sign</li>
  </ul>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <x-sortable name="publications" :list="$listing->publications"/>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.publication.google', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
