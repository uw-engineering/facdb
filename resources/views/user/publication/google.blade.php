@extends('user.publication.base')
@section('title', 'Publications - Google Scholar')

@section('breadcrumb')
  @parent
  <li class="breadcrumb-item">Google Scholar</li>
@endsection

@section('card')

  <h2 class="card-title">Google Scholar</h2>
  <h6 class="card-subtitle text-muted">
  We recommend providing a link to your Google Scholar Profile so you can curate an exhaustive list of your publications. </h6>
  <p>Learn how to <a href="https://scholar.google.com/intl/en/scholar/citations.html">create or edit your google scholar profile</a>. </p>
  <p><strong>Note that on the next page you can specify up to 20 select publications to include on your faculty page.</strong></p>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post">
    {{ csrf_field() }}
    
    <div class="form-group row">
      <label for="email" class="col-sm-3 col-form-label">Account URL</label>
      <div class="col-sm-9">
        <input type="text" 
          class="form-control" 
          id="google_scholar" 
          name="google_scholar" 
          placeholder="Account URL" 
          value="{{ old('google_scholar', $listing->google_scholar) }}"
        >
      </div>
    </div>

    <div class="form-group row">
      <div class="col">
        <a href="{{ route('user.affiliation.list', $listing) }}" class="btn btn-secondary">Previous</a>
      </div>
      <div class="col">
        <div class="d-flex flex-row-reverse">
          <button id="next" type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
      </div>
    </div>

  </form>

@endsection
