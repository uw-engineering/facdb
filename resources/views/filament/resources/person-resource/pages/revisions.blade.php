<x-filament-panels::page>

    <x-filament::section>
        <x-slot name="heading">Bio</x-slot>
        <x-revisions :record="$this->record" />
    </x-filament::section>
        
    <x-filament::section>
        <x-slot name="heading">Appointments</x-slot>

        @foreach ($this->record->appointmentsWithTrashed as $appointment)
            <x-filament::section>
                <x-slot name="heading">
                    {{ ($appointment->department ? $appointment->department->name : $appointment->other_department) . ' ' . $appointment->title }}
                </x-slot>
                @if ($appointment->revised)
                    <x-revisions :record="$appointment" />
                @endif
            </x-filament::section>
            <br>
        @endforeach
    </x-filament::section>

    <x-filament::section>
        <x-slot name="heading">Listings</x-slot>

        @foreach ($this->record->listingsWithTrashed as $listingUsed)
            @if ($listingUsed->revised)
                <x-filament::section>
                    <x-slot name="heading">{{ $listingUsed->department->name }}</x-slot>
                    
                    <x-revisions :record="$listingUsed" />

                    @foreach ($listingUsed->affiliationsWithTrashed as $affiliation)
                        @if ($affiliation->revised)
                            <x-revisions :record="$affiliation" />
                        @endif
                    @endforeach

                    @foreach ($listingUsed->projectsWithTrashed as $project)
                        @if ($project->revised)
                            <x-revisions :record="$project" />
                        @endif
                    @endforeach
                </x-filament::section>
                <br>
            @endif
        @endforeach

    </x-filament::section>

</x-filament-panels::page>
