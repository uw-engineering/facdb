@extends('dashboard::page')
@section('title', 'UW CoE FacDB API Documentation')
@section('navbar-text', 'UW CoE Faculty Database')

@section('head')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
@endsection

@section('scripts')
  @parent
  <script src="{{ asset(mix('js/app.js')) }}"></script>
@endsection

@section('navbar')
<ul class="navbar-nav mr-auto">
  <li class="nav-item">
    <a class="nav-link" href="#">Faculty Database API Documentation</a>
  </li>
</ul>
@endsection

@section('sidebar')
  <ul class="nav nav-pills flex-column">

    @foreach ($pages as $page)
      <li class="nav-item {{ ends_with(url()->current(), $page) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('docs/' . $page) }}">
          {{ ucwords($page) }}
        </a>
      </li>
    @endforeach

  </ul>
@endsection
