@extends('docs.html')

@section('main')
  <h2>People</h2>
  <p>
    Returns json data for a list of faculty members.
  </p>
  <h3>URL</h3>
  <code>/people</code>
  <h3>Method</h3>
  <code>GET</code>
@endsection
