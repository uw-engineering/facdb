
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Sortable = require('sortablejs').Sortable;

$(window).on('load', function () {

    $('.dynamic-text-input input').on('keyup', function () {
        
        const addButton = $(this).parents('.form-group').find('.add-more')
        const lastInput = $(this).parents('ul').children().last().find('input')[0]

        // Was the input change on the last one in the list?
        if (lastInput == $(this)[0]) {

            // Hide the add button if the last element is empty.
            if ($(this).val() == '') {
                addButton.hide()
            } else {
                addButton.show()
            }
        }
    })

    $('.dynamic-text-input-remove button').on('click', removeEvent)

    $('.add-more').on('click', function () {
        const last = $(this).parent().parent().find('li').last()

        var el = last.clone(true, true)
        el.find('input').val('')
        el.insertAfter(last)

        el.parent().children().each(function () {
            addRemove($(this))
            addHandle($(this))
        })

        // Hide add more button.
        $(this).hide()
    })

    function removeEvent() {
        const ul = $(this).parents('.sortable-list')

        // Remove the list item.
        $(this).parents('li.dynamic-text-input').remove()

        // Cleanup if needed on the others.
        if (ul.children().length == 1) {
            toggleHandle(ul.children())
            toggleRemove(ul.children())
        }
    }

    function toggleHandle(el) {
        var handle = el.children('.handle')
        if (handle.length == 0) {
            addHandle(el)
        } else {
            handle.remove()
        }
    }
    function addHandle(el) {
        var handle = el.children('.handle')
        if (handle.length == 0) {
            const html = `
                <div class="input-group-append handle">
                    <a class="btn btn-secondary" type="button">
                        <i class="fas fa-arrows-alt fa-fw" aria-hidden="true"></i>
                    </a>
                </div>
            `;
            $(html).insertAfter(el.children('.form-control'))
        }
    }

    function toggleRemove(el) {
        var remove = el.children('.dynamic-text-input-remove')
        if (remove.length == 0) {
            addRemove(el)
        } else {
            remove.remove()
        }
    }
    function addRemove(el) {
        var remove = el.children('.dynamic-text-input-remove')
        if (remove.length == 0) {
            const html = `
                <div class="input-group-append dynamic-text-input-remove">
                    <button class="btn btn-primary" type="button">
                        <i class="fas fa-minus fa-fw" aria-hidden="true"></i>
                    </button>
                </div>
            `;
            $(html).insertAfter(el.children('.form-control')).on('click', removeEvent)
        }
    }
});
