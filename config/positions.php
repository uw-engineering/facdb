<?php

return [

    'titles' => [
        'Professor',
        'Associate Professor',
        'Assistant Professor',
        'Research Professor',
        'Research Associate Professor',
        'Research Assistant Professor',
        'Professor Emeritus',
        'Associate Professor Emeritus',
        'Research Professor Emeritus',
        'Lecturer',
        'Senior Lecturer',
        'Assistant Teaching Professor',
        'Associate Teaching Professor',
        'Full Teaching Professor',
    ]
];
