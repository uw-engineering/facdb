<?php

return array(

    'enabled' => env('NETID_AUTH_ENABLED', true),
    'mock' => env('NETID_AUTH_MOCK', false),

    'login_url' => '/Shibboleth.sso/Login',
    'logout_url' => '/Shibboleth.sso/Logout',

    'handler' => 'App\Models\LoginHandler',
);
