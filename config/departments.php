<?php

return [

    'aa' => [
        'name' => 'Aeronautics & Astronautics',
        'abbreviation' => 'A&A'
    ],

    'bioe' => [
        'name' => 'Bioengineering',
        'abbreviation' => 'BioE'
    ],

    'bse' => [
        'name' => 'Bioresource Science & Engineering',
        'abbreviation' => 'BSE'
    ],

    'cheme' => [
        'name' => 'Chemical Engineering',
        'abbreviation' => 'ChemE'
    ],

    'cee' => [
        'name' => 'Civil & Environmental Engineering',
        'abbreviation' => 'CEE'
    ],

    'cse' => [
        'name' => 'Computer Science & Engineering',
        'abbreviation' => 'CSE'
    ],

    'ee' => [
        'name' => 'Electrical & Computer Engineering',
        'abbreviation' => 'ECE'
    ],

    'hcde' => [
        'name' => 'Human Centered Design & Engineering',
        'abbreviation' => 'HCDE'
    ],

    'ise' => [
        'name' => 'Industrial & Systems Engineering',
        'abbreviation' => 'ISE'
    ],

    'mse' => [
        'name' => 'Materials Science & Engineering',
        'abbreviation' => 'MSE'
    ],

    'me' => [
        'name' => 'Mechanical Engineering',
        'abbreviation' => 'ME'
    ]
];
