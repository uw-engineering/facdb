<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\Department;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Person::all()->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return $person->toJson();
    }    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function departments(Person $person)
    {
        return $person->listings()->get()->map(function ($listing, $key) {
            return $listing->department()->first();
        })->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person      $person
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function department_listing(Person $person, Department $department)
    {
        return $person->listings()->get()->filter(function ($listing, $key) use ($department) {
            return $listing->department()->first()->is($department);
        })->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function listings(Person $person)
    {
        return $person->listings()->get()->toJson();
    }
}
