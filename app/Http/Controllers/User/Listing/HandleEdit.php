<?php

namespace App\Http\Controllers\User\Listing;

use App\Models\Department;
use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleEdit extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        if ($request->isMethod('post')) {
            
            $listing->department_url = $request->input('department_url');
            $listing->save();

            return redirect()->route('user.listing.list');
        }

        return view('user.listing.edit', [
            'listing' => $listing,
            'departments' => Department::all()
        ]);
    }
}
