<?php

namespace App\Http\Controllers\User\Listing;

use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleList extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
     public function __invoke(Request $request, Person $person)
     {
        $listing = $person->listings->first();

        return view('user.listing.list', ['listing' => $listing]);
     }
}
