<?php

namespace App\Http\Controllers\User\Listing;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleDelete extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        $listing->delete();

        return redirect()->route('user.listing.list');
    }
}
