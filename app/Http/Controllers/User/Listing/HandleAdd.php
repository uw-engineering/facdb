<?php

namespace App\Http\Controllers\User\Listing;

use App\Models\Person;
use App\Models\Listing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleAdd extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        if ($request->isMethod('post')) {
            $listing = new Listing();
            $listing->department_id = $request->input('department');
            $listing->person_id = $person->id;
            $listing->department_url = $request->input('department_url');
            $listing->save();

            return redirect()->route('user.listing.list');
        }

        return view('user.listing.add');
    }
}
