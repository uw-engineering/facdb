<?php

namespace App\Http\Controllers\User;

use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use UWCoE\PWS\Person as PWS;
use Illuminate\Support\Facades\Auth;

class HandleWelcome extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request)
    {
        // This page is for first-time users, so if they end up here
        // go ahead and create a person object for them or grab
        // one from the db if they are already in it
        $user = $request->user();
        $shib = $user->getShibbolethParameters();
        $uwnetid = $shib['uwnetid'];
        $employeeId = $shib['employeeNumber'];
        $person = Person::where('uwnetid', $uwnetid)
            ->whereNotNull('uwnetid')
            ->first();

        // This person's netid was not found, so they are either a
        // first-time editor or they are not in the system. Try
        // to find them from their PWS data and match
        if (!$person) {
            $person = Person::create([
                'uwnetid' => $uwnetid,
                'employee_id' => $employeeId,
            ]);
            $person->hash = $person->generateHash();
            $person->save();
        }

        return view('user.welcome', ['person' => $person]);
    }

    protected function updatePersonWithPWSData($pws, $person)
    {
        // Save the netid now so this process is avoided in the future
        $person->employee_id = $pws->PersonAffiliations->EmployeePersonAffiliation->EmployeeID;
        $person->uwnetid = $pws->UWNetID;
        $person->reg_id = $pws->UWRegID;
        $person->save();

        return $person;
    }

    protected function findPersonFromPWS($uwnetid)
    {
        $pws = PWS::fromNetId($uwnetid);

        // All faculty should have an employee affiliation with UW
        if (!isset($pws->PersonAffiliations->EmployeePersonAffiliation)) {
            return null;
        }

        // Use their employee affiliation to get their desired full name
        // TODO: Make sure that this data is always available or change
        // how this is determined
        $employee = $pws->PersonAffiliations->EmployeePersonAffiliation;

        // First check if they have a matching employee id number
        $person = Person::where('employee_id', $employee->EmployeeID)->first();
        if ($person) {
            return $this->updatePersonWithPWSData($pws, $person);
        }

        // Next check whitepage info
        if (!isset($employee->EmployeeWhitePages)) {
            return null;
        }

        $desiredName = $employee->EmployeeWhitePages->Name;
        $nameComponents = explode(',', $desiredName);
        $fullName = trim($nameComponents[0]);
        if (count($nameComponents) == 2) {
            $fullName = trim($nameComponents[1]) . ' ' . $fullName;
        }
        // In case someone decides to use a weird character
        $fullName = iconv('UTF-8', 'ASCII//TRANSLIT', $fullName);
        // Remove all spacing
        $fullName = str_replace(' ', '', $fullName);
        
        $matches = $this->match($fullName);

        if (count($matches) != 1) {
            return null;
        } else {
            return $this->updatePersonWithPWSData($pws, $matches[0]);
        }
    }

    protected function match($nameWithoutSpace)
    {
        // run name matching on the entire list
        $people = Person::all();
        $matches = [];
        foreach ($people as $per) {
            $checkName = $per->first_name;
            if ($per->middle_name) {
                $checkName .= ' ' . $per->middle_name;
            }
            $checkName .= ' ' . $per->last_name;
            $checkName = iconv('UTF-8', 'ASCII//TRANSLIT', $checkName);
            // Remove nicknames in quotes
            $checkName = preg_replace('/".*?"/', '', $checkName);
            $checkName = str_replace(' ', '', $checkName);

            if ($checkName == $nameWithoutSpace) {
                $matches[] = $per;
            }
        }

        return $matches;
    }
}
