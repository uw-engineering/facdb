<?php

namespace App\Http\Controllers\User\Appointment;

use App\Models\Appointment;
use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;

class HandleDelete extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Appointment $appointment)
    {
        $appointment->delete();

        // Potentially also remove some listings here

        return redirect()->route('user.appointment.list');
    }
}
