<?php

namespace App\Http\Controllers\User\Appointment;

use App\Models\Appointment;
use App\Models\Department;
use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleAdd extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        if ($request->isMethod('post')) {

            $appointment = new Appointment();
            $appointment->endowed_title = $request->input('endowed_title');
            $appointment->adjunct = $request->has('adjunct');
            $appointment->affiliate = $request->has('affiliate');
            $appointment->title = $request->input('title');
            $appointment->person_id = $person->id;

            if ($request->input('department')) {
                $appointment->department_id = $request->input('department');
            } else {
                $appointment->other_department = $request->input('other_department');
            }

            $appointment->save();

            return redirect()->route('user.appointment.list');
        }

        return view('user.appointment.add', [
            'departments' => Department::all()
        ]);
    }
}
