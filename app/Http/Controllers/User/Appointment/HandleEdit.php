<?php

namespace App\Http\Controllers\User\Appointment;

use App\Models\Appointment;
use App\Models\Department;
use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;

class HandleEdit extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Appointment $appointment)
    {   
        if ($request->isMethod('post')) {

            $appointment->endowed_title = $request->input('endowed_title');
            $appointment->adjunct = $request->has('adjunct');
            $appointment->affiliate = $request->has('affiliate');
            $appointment->title = $request->input('title');
            $appointment->person_id = $person->id;

            if ($request->input('department')) {
                $appointment->department_id = $request->input('department');
            } else {
                $appointment->other_department = $request->input('other_department');
            }

            $appointment->save();

            return redirect()->route('user.appointment.list');
        }

        return view('user.appointment.edit', [
            'appointment' => $appointment,
            'departments' => Department::all()
        ]);
    }
}
