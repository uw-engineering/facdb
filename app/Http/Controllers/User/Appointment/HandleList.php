<?php

namespace App\Http\Controllers\User\Appointment;

use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleList extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        return view('user.appointment.list', ['appointments' => $person->appointments]);
    }
}
