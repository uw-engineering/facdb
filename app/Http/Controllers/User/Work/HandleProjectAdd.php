<?php

namespace App\Http\Controllers\User\Work;

use App\Models\Listing;
use App\Models\Project;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleProjectAdd extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            $project = new Project();
            $project->name = $request->input('name');
            $project->description = $request->input('description');
            $project->listing_id = $listing->id;
            $project->save();

            return redirect()->route('user.work.project.list', $listing);
        }

        return view('user.work.project.add', $this->viewData);
    }
}
