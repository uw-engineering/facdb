<?php

namespace App\Http\Controllers\User\Work;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleCourses extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            // All of these attributes are expected to be arrays
            $listing->courses = collect($request->input('courses'))->filter();
            $listing->save();

            return redirect()->route('user.work.project.list', $listing);
        }

        return view('user.work.courses', $this->viewData);
    }
}
