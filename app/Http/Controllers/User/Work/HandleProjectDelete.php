<?php

namespace App\Http\Controllers\User\Work;

use App\Models\Listing;
use App\Models\Project;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleProjectDelete extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing, Project $project)
    {
        $project->delete();
        return redirect()->route('user.work.project.list', $listing);
    }
}
