<?php

namespace App\Http\Controllers\User\Work;

use App\Models\Listing;
use App\Models\Project;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleProjectEdit extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing, Project $project)
    {
        self::getData($listing);

        $this->viewData['project'] = $project;

        if ($request->isMethod('post')) {
            $project->name = $request->input('name');
            $project->description = $request->input('description');
            $project->save();

            return redirect()->route('user.work.project.list', $listing);
        }

        return view('user.work.project.edit', $this->viewData);
    }
}
