<?php

namespace App\Http\Controllers\User\Basic;

use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;

class HandleName extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required'
            ]);

            $person->first_name = $request->input('first_name');
            $person->last_name = $request->input('last_name');
            $person->middle_name = $request->input('middle_name');
            $person->pronouns= $request->input('pronouns');
            $person->save();

            return redirect()->route('user.basic.photo');
        }
        return view('user.basic.name');
    }
}
