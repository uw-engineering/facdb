<?php

namespace App\Http\Controllers\User\Basic;

use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;

class HandleNext extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        return view('user.basic.next');
    }
}
