<?php

namespace App\Http\Controllers\User\Basic;

use App\Events\ModelSynced;
use App\Http\Controllers\Controller;
use App\Models\FileReference;
use App\Models\Person;
use App\Services\RevisionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use UWCoE\FileDB\File;

class HandlePhoto extends Controller
{
    public function __invoke(Request $request, Person $person, RevisionService $service)
    {
        if ($request->isMethod('post')) {
            
            if (!$person->photo) {
                $this->validate($request, [
                    'photo' => 'required'
                ]);
            }

            if ($request->hasFile('photo')) {
                $photo = File::createFromUploadedFile($request->file('photo'));
                
                $old_photo = $person->photo?->name;
                $person->files()->save($photo, ['usage' => 'photo']);
                // $person->load('files');
                // $new_photo = $person->photo->name;

                $service->addRevision(
                    'photo',
                    $person,
                    Person::class,
                    $person->id,
                    $old_photo,
                    $photo->name,
                );
            }

            return redirect()->route('user.basic.contact');
        }

        $photo = $person->photo;
        return view('user.basic.photo', [
            'photoUrl' => $person->photo ?
                route('file', [$person->photo]) : 
                asset('images/photo-default.png'),
            'photoTitle' => $person->photo ?
                $person->photo->name : 'Choose file...'
        ]);
    }
}
