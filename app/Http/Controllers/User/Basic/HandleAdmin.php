<?php

namespace App\Http\Controllers\User\Basic;

use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;

class HandleAdmin extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        if ($request->isMethod('post')) {

            // All of these attributes are expected to be arrays
            $person->title = collect($request->input('title'))->filter();
            $person->save();

            return redirect()->route('user.basic.next');
        }
        return view('user.basic.admin');
    }
}
