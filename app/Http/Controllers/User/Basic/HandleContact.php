<?php

namespace App\Http\Controllers\User\Basic;

use App\Models\Building;
use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HandleContact extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'email' => 'email'
            ]);

            $person->email = $request->input('email');
            if (!$person->uwnetid && Str::endsWith($person->email, '@uw.edu')) {
                $person->uwnetid = explode('@', $person->email)[0];
            }
            $person->office_number = $request->input('office_number');
            $person->office_building = $request->input('office_building');
            $person->phone = $request->input('phone');
            $person->website_url = $request->input('website_url');
            $person->social = collect($request->input('social'))->filter();
            $person->save();

            return redirect()->route('user.basic.admin');
        }
        return view('user.basic.contact', ['buildings' => Building::all()]);
    }
}
