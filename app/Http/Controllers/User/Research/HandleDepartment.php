<?php

namespace App\Http\Controllers\User\Research;

use App\Models\Listing;
use App\Models\Person;
use App\Models\DepartmentResearchArea;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleDepartment extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
     public function __invoke(Request $request, Person $person, Listing $listing)
     {
         self::getData($listing);
         
         $departmentId = $listing->department->id;
         $this->viewData['areas'] = 
            DepartmentResearchArea::where('department_id', $departmentId)->get();
 
         if ($request->isMethod('post')) {
             $listing->syncResearchAreas('department', array_keys($request->input('department', [])));
 
             return redirect()->route('user.research.personal', $listing);
         }
         
         return view('user.research.department', $this->viewData);
     }
}
