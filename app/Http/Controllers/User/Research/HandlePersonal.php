<?php

namespace App\Http\Controllers\User\Research;

use App\Models\Listing;
use App\Models\Person;
use App\Models\ResearchArea;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandlePersonal extends BaseListingHandler
{
    /**
    * Show the profile for the given user.
    *
    * @param  int  $id
    * @return Response
    */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        $this->getData($listing);
         
        if ($request->isMethod('post')) {
            $toSync = [];
            $order = 1;
            foreach ($request->input('personal', []) as $personal) {
                if ($personal) {
                    $id = ResearchArea::firstOrCreate([
                        'name' => $personal
                    ])->id;
                    $toSync[$id] = ['order' => $order];
                    $order++;
                }
            }

            $listing->personalResearchAreas()->sync($toSync);
 
            return redirect()->route('user.research.statement', $listing);
        }
         
        return view('user.research.personal', $this->viewData);
    }
}
