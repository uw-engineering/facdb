<?php

namespace App\Http\Controllers\User\Research;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleStatement extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);
        
        if ($request->isMethod('post')) {
            // All of these attributes are expected to be arrays
            $listing->research_statement = $request->input('research_statement');
            $listing->save();

            return redirect()->route('user.affiliation.list', $listing);
        }
        
        return view('user.research.statement', $this->viewData);
    }
}
