<?php

namespace App\Http\Controllers\User\Research;

use App\Models\Listing;
use App\Models\Person;
use App\Models\StrategicResearchArea;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleStrategic extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);
        
        $this->viewData['areas'] = StrategicResearchArea::all();

        if ($request->isMethod('post')) {
            $listing->strategicResearchAreas()->sync(array_keys($request->input('strategic', [])));

            return redirect()->route('user.research.department', $listing);
        }
        
        return view('user.research.strategic', $this->viewData);
    }
}
