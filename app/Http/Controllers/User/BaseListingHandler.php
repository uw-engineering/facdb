<?php

namespace App\Http\Controllers\User;

use App\Models\Person;
use App\Models\Listing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseListingHandler extends Controller
{
    protected $person;
    protected $listing;
    protected $viewData;

    protected function getData($listing = null)
    {
        $this->listing = $listing;
        // $this->person = Person::fromAuth();
        // $this->listing = $listing ? $listing : $this->person->listings->first();
        
        $this->viewData = [
            'listing' => $this->listing
        ];
    }
}
