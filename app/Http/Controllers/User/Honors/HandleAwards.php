<?php

namespace App\Http\Controllers\User\Honors;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleAwards extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            // All of these attributes are expected to be arrays
            $listing->awards = collect($request->input('awards'))->filter();
            $listing->save();

            return redirect()->route('user.honors.fellowships', $listing);
        }

        return view('user.honors.awards', $this->viewData);
    }
}
