<?php

namespace App\Http\Controllers\User\History;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleAcademics extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            // All of these attributes are expected to be arrays
            $listing->doctorate = collect($request->input('doctorate'))->filter();
            $listing->masters = collect($request->input('masters'))->filter();
            $listing->bachelors = collect($request->input('bachelors'))->filter();
            $listing->save();

            return redirect()->route('user.history.appointments', $listing);
        }

        return view('user.history.academics', $this->viewData);
    }
}
