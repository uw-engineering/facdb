<?php

namespace App\Http\Controllers\User\History;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleBio extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);
        
        if ($request->isMethod('post')) {
            $listing->bio = $request->input('bio');
            $listing->save();

            return redirect()->route('user.history.cv', $listing);
        }
        
        return view('user.history.bio', $this->viewData);
    }
}
