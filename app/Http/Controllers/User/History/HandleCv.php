<?php

namespace App\Http\Controllers\User\History;

use App\Models\Cv;
use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use App\Services\RevisionService;
use Illuminate\Http\Request;

class HandleCv extends BaseListingHandler
{
    public function __invoke(Request $request, Person $person, Listing $listing, RevisionService $service)
    {
        self::getData($listing);
        
        if ($request->isMethod('post')) {

            if ($request->input('delete')) {
                $this->clearCvs($person, $service);
                return redirect()->route('user.history.cv', $listing);
            }

            if ($request->hasFile('cv')) {
                
                $this->clearCvs($person, $service);
                
                $cv = Cv::createFromUploadedFile($request->file('cv'), Cv::class);
                $person->cvs()->save($cv, ['usage' => 'cv']);
                
                $service->addRevision(
                    'cv',
                    $person,
                    Person::class,
                    $person->id,
                    null,
                    $cv->name
                );

                // // To trigger a revision with a person attached.
                // $cv->updated_at = now()->addSecond();
                // $cv->save();
            }

            return redirect()->route('user.research.strategic', $listing);
        }
        
        return view('user.history.cv', $this->viewData);
    }

    protected function clearCvs(Person $person, RevisionService $service)
    {
        foreach ($person->cvs as $cv) {
            $person->cv->delete();
            $person->files()->detach($cv->id);
            $service->addRevision(
                'cv',
                $person,
                Person::class,
                $person->id,
                $cv->name,
                null
            );
        }
    }
}
