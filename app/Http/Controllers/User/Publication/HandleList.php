<?php

namespace App\Http\Controllers\User\Publication;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleList extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            // All of these attributes are expected to be arrays
            $listing->publications = collect($request->input('publications'))->filter();
            $listing->save();

            return redirect()->route('user.work.courses', $listing);
        }

        return view('user.publication.list', $this->viewData);
    }
}
