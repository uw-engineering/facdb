<?php

namespace App\Http\Controllers\User\Publication;

use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleGoogle extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        if ($request->isMethod('post')) {
            $listing->google_scholar = $request->input('google_scholar');
            $listing->save();

            return redirect()->route('user.publication.list', $listing);
        }

        return view('user.publication.google', $this->viewData);
    }
}
