<?php

namespace App\Http\Controllers\User\Affiliation;

use App\Models\Affiliation;
use App\Models\DepartmentResearchArea;
use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleAdd extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
     public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        $this->viewData['areas'] = 
            DepartmentResearchArea::where('department_id', $listing->department->id)->get();

        if ($request->isMethod('post')) {
            $affiliation = new Affiliation();
            $affiliation->type = $request->input('type');
            $affiliation->name = $request->input('name');
            $affiliation->website = $request->input('website');
            $affiliation->phone = $request->input('phone');
            $affiliation->location = $request->input('location');
            $affiliation->listing_id = $listing->id;
            $affiliation->save();

            $affiliation->researchAreas()->sync(array_keys($request->input('areas', [])));

            return redirect()->route('user.affiliation.list', $listing);
        }

        return view('user.affiliation.add', $this->viewData);
    }
}
