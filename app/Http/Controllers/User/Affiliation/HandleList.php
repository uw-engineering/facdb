<?php

namespace App\Http\Controllers\User\Affiliation;

use App\Models\Person;
use App\Models\Listing;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleList extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing)
    {
        self::getData($listing);

        return view('user.affiliation.list', $this->viewData);
    }
}
