<?php

namespace App\Http\Controllers\User\Affiliation;

use App\Models\Affiliation;
use App\Models\Listing;
use App\Models\Person;
use App\Http\Controllers\User\BaseListingHandler;
use Illuminate\Http\Request;

class HandleDelete extends BaseListingHandler
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke(Request $request, Person $person, Listing $listing, Affiliation $affiliation)
    {
        $affiliation->delete();
        return redirect()->route('user.affiliation.list', $listing);
    }
}
