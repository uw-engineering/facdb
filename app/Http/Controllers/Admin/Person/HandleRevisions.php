<?php

namespace App\Http\Controllers\Admin\Person;

use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleRevisions extends Controller
{
    /**
    * Show the profile for the given user.
    *
    * @param  int  $id
    * @return Response
    */
    public function __invoke(Request $request, Person $person)
    {
        return view('admin.person.revisions');
    }
}
