<?php

namespace App\Http\Controllers\Admin\Person;

use App\Filament\Resources\PersonResource;
use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandlePublish extends Controller
{
    /**
    * Show the profile for the given user.
    */
    public function __invoke(Request $request, Person $person)
    {
        $person->publish();

        return redirect(PersonResource::getUrl('index'));
    }
}
