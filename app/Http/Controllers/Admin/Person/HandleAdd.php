<?php

namespace App\Http\Controllers\Admin\Person;

use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use UWCoE\PWS\Person as PWS;

class HandleAdd extends Controller
{
    public function __invoke(Request $request)
    {
        if ($request->isMethod('post')) {

            if ($request->input('uwregid')) {
                $person = self::createPWSPersonFromRegId($request->input('uwregid'));
                return redirect()->route('user.basic.name', $person);
            } else if ($request->input('uwnetid')) {
                $person = self::createPWSPersonFromNetId($request->input('uwnetid'));
                return redirect()->route('user.basic.name', $person);
            } else {
                $pws = PWS::search([
                    'first_name' => $request->input('first_name') . '*', 
                    'last_name' => $request->input('last_name') . '*',
                    'edupersonaffiliation_faculty' => 'true',
                    'page_size' => 3,
                ]);
                $request->flash();
                return view('admin.person.add', ['search' => $pws]);
            }
        }

        return view('admin.person.add');
    }

    protected function createPWSPersonFromRegId($regId)
    {
        $pws = PWS::fromRegId($regId);
        return self::createPWSPerson($pws);
    }

    protected function createPWSPersonFromNetId($uwnetid)
    {
        $pws = PWS::fromNetId($uwnetid);
        return self::createPWSPerson($pws);
    }

    protected function createPWSPerson($pws)
    {
        if ($pws == null) {
            throw new \Exception('Person not found');
        }
            
        $person = Person::firstOrNew(['reg_id' => $pws->UWRegID]);
            
        $person->uwnetid = $pws->UWNetID;
        $person->first_name = $pws->PreferredFirstName ? $pws->PreferredFirstName : current(explode(' ', $pws->RegisteredName));
        $person->last_name = $pws->PreferredSurname ? $pws->PreferredSurname : $pws->RegisteredSurname;
        $person->middle_name = $pws->PreferredMiddleName ? $pws->PreferredMiddleName : null;
        $person->email = $pws->UWNetID . '@uw.edu';
        $person->employee_id = $pws->PersonAffiliations->EmployeePersonAffiliation->EmployeeID;
        $person->title = [];
        $person->save();

        return $person;
    }
}
