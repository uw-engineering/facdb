<?php

namespace App\Http\Controllers\Admin\Person;

use App\Filament\Resources\PersonResource;
use App\Models\Person;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleDelete extends Controller
{
    /**
    * Show the profile for the given user.
    */
    public function __invoke(Request $request, Person $person)
    {
	    $person->unpublish();
        $person->delete();

        return redirect(PersonResource::getUrl('index'));
    }
}
