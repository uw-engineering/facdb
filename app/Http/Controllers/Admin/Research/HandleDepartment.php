<?php

namespace App\Http\Controllers\Admin\Research;

use App\Models\Department;
use App\Models\DepartmentResearchArea;
use App\Http\Controllers\Controller;
use App\Models\ResearchArea;
use Illuminate\Http\Request;

class HandleDepartment extends Controller
{
    /**
    * Show the profile for the given user.
    *
    * @param  int  $id
    * @return Response
    */
    public function __invoke(Request $request, Department $department)
    {
        if ($request->isMethod('post')) {
            $areaIds = [];
            foreach ($request->input('research', []) as $research) {
                if ($research) {
                    $areaIds[] = ResearchArea::firstOrCreate([
                        'name' => ucwords($research)
                    ])->id;
                }
            }

            $department->researchAreas()->sync($areaIds);
 
            return redirect()->route('admin.research.select');
        }

        return view('admin.research.department', ['department' => $department]);
    }
}
