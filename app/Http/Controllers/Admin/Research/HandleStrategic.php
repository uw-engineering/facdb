<?php

namespace App\Http\Controllers\Admin\Research;

use App\Http\Controllers\Controller;
use App\Models\ResearchArea;
use App\Models\StrategicResearchArea;
use Illuminate\Http\Request;

class HandleStrategic extends Controller
{
    /**
    * Show the profile for the given user.
    *
    * @param  int  $id
    * @return Response
    */
    public function __invoke(Request $request)
    {
        $original = StrategicResearchArea::all();
        $originalIds = $original->pluck('research_area_id')->all();

        if ($request->isMethod('post')) {
            $syncIds = [];
            foreach ($request->input('research', []) as $research) {
                if ($research) {
                    $syncIds[] = ResearchArea::firstOrCreate([
                        'name' => ucwords($research)
                    ])->id;
                }
            }

            foreach ($original as $area) {
                if (!in_array($area->id, $syncIds)) {
                    $area->delete();
                }
            }
        
            foreach ($syncIds as $id) {
                if (!in_array($id, $originalIds)) {
                    StrategicResearchArea::create(['research_area_id' => $id]);
                }
            }

            return redirect()->route('admin.research.select');
        }

        return view('admin.research.strategic', ['strategicResearchAreas' => $original->pluck('researchArea')]);
    }
}
