<?php

namespace App\Http\Controllers\Admin\Research;

use App\Models\Department;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleSelect extends Controller
{
    /**
    * Show the profile for the given user.
    *
    * @param  int  $id
    * @return Response
    */
    public function __invoke(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->has('department')) {
                return redirect()->route('admin.research.department', Department::findOrFail($request->input('department')));
            }
        }

        return view('admin.research.select', ['departments' => Department::all()]);
    }
}
