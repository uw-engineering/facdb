<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Listing::all()->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        return $listing->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function affiliations(Listing $listing)
    {
        return $listing->affiliations()->get()->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function department(Listing $listing)
    {
        return $listing->department()->get()->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function honors(Listing $listing)
    {
        return $listing->honors()->get()->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function person(Listing $listing)
    {
        return $listing->person()->first()->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function publications(Listing $listing)
    {
        return $listing->publications()->get()->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function research_areas(Listing $listing)
    {
        return $listing->research_areas()->get()->toJson();
    }
}
