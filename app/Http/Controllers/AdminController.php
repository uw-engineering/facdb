<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function list(Request $request)
    {
        if ($request->has("sort")) {
            $sort = $request->query('sort');
            if ($request->query('flip')) {
                $order = strcmp($request->query('order'), 'asc') == 0 ? 'desc' : 'asc';
            } else {
                if (strcmp($sort, 'uwnetid') == 0 || strcmp($sort, 'last_name') == 0) {
                    $order = 'asc';
                } else {
                    $order = 'desc';
                }
            }
        } else {
            $sort = 'last_name';
            $order ='asc';
        }
        $search = $request->input('search');
        $people = \App\Models\Person::with([
            'listingsWithTrashed',
            'listingsWithTrashed.projectsWithTrashed',
            'listingsWithTrashed.affiliationsWithTrashed',
            'listingsWithTrashed.revisionHistory',
            'appointmentsWithTrashed',
            'appointmentsWithTrashed.revisionHistory',
            'appointmentsWithTrashed.person',
            'revisionHistory',
            'files',
        ])
        ->where('first_name', 'like', '%'.$search.'%')
        ->orWhere('last_name', 'like', '%'.$search.'%')
        ->orderBy($sort, $order)
        ->paginate(20)
        // Hold on to the other GET parameters in the links
        ->appends($request->all());
        return view('admin.list', [
            'people' => $people,
            'search' => $search,
            'request'=> $request,
            'order' => $order
        ]);
    }
}
