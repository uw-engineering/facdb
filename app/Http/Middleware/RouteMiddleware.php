<?php

namespace App\Http\Middleware;

use App\Models\Person;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;

class RouteMiddleware
{
    public function handle($request, Closure $next)
    {
        $person = $request->route()->person;
        
        // if (!$person) {
        //     $person = Person::fromAuth();
        //     $request->route()->setParameter('person', $person);
        // }
        
        if ($person) {
            URL::defaults(['person' => $person->hash]);
            View::composer('*', function ($view) use ($person) {
                $view->with('person', $person);
            });
        }
        // $person = Person::fromAuth();
        // URL::defaults(['person' => $person]);

        return $next($request);
    }
}
