<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use UWCoE\NetId\Middleware\NetId as UWCoENetId;

class NetId extends UWCoENetId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$requires)
    {
        // Run the basic netid middleware first
        $response = call_user_func_array([$this, 'parent::handle'], array_merge([$request, $next], $requires));
        //$response = parent::handle($request, $next, $requires);

        // If this is the first time this middleware is invoked,
        // a redirect will be required in order to get netid data
        // so just skip the whole process and return the redirect
        // response instead.
        if (!is_a($response, RedirectResponse::class)) {

            $user = Auth::user();

            // If this request only requires passive auth, move on
            if (!$user) {
                return $response;
            }
            
            // Block all app access to only 99s
            // if ($user->level != 99) {
            //     abort(403, 'Unauthorized action.');
            // }

            // Redirect to Admin Page if masquerade has expired
            // if (\Illuminate\Support\Str::startsWith(Route::currentRouteName(), 'user') && !session('masquerade')) {
            //     return redirect()->route('admin.list');
            // }
        }
        
        return $response;
    }
}
