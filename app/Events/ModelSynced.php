<?php

namespace App\Events;
use Illuminate\Foundation\Events\Dispatchable;

class ModelSynced
{
    use Dispatchable;

    public $pivot;
    public $add;
    public $remove;

    /**
     * Create a new event instance.
     */
    public function __construct($pivot, $changes)
    {
        $this->pivot = $pivot;
        $this->add = $changes['add'];
        $this->remove = $changes['remove'];
    }
}
