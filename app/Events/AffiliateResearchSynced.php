<?php

namespace App\Events;

use App\Affiliate;
use Illuminate\Queue\SerializesModels;

class AffiliateResearchSynced
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
