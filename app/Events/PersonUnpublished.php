<?php

namespace App\Events;

use App\Models\PublishedPerson;
use App\Events\Traits\PurgesDrupalCaches;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class PersonUnpublished
{
    use Dispatchable, SerializesModels, PurgesDrupalCaches;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PublishedPerson $publishedPerson)
    {
        $this->purgeDrupalCaches($publishedPerson);
    }
}
