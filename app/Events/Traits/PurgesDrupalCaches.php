<?php

namespace App\Events\Traits;

use App\Models\PublishedPerson;
use GuzzleHttp\Client;

trait PurgesDrupalCaches
{
    protected $endpoint = '/facultyfinder/purge/';

    public function purgeDrupalCaches(PublishedPerson $publishedPerson)
    {
        $listings = $publishedPerson->person['listings'];
        foreach ($listings as $listing) {
            $url = $listing['department']['url'] . $this->endpoint;
            $profileUrl = $url . 'profile/' . $publishedPerson->hash;
            $searchUrl = $url . 'search';

            $this->purge($profileUrl);
            $this->purge($searchUrl);
        }
    }

    protected function purge($url)
    {
        $client = new Client();

        try {
            $client->get($url);
        } catch (\Exception $e) {
            return;
        }
    }
}

