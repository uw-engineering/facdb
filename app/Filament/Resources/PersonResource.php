<?php

namespace App\Filament\Resources;

use App\Filament\Resources\Person\Pages;
use App\Models\Person;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class PersonResource extends Resource
{
    protected static ?string $model = Person::class;

    protected static ?string $navigationIcon = 'heroicon-s-credit-card';

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('photoUrl')
                    ->label(false)
                    ->state(fn (Person $record) => "$record->photoUrl/40/40")
                    ->size(40)
                    ->circular(),
                Tables\Columns\TextColumn::make('name')
                    ->sortable(['first_name', 'last_name'])
                    ->searchable(['first_name', 'last_name', 'uwnetid'])
                    ->description(fn ($record) => $record->appointments->first()->title ?? null),
                Tables\Columns\TextColumn::make('listings.department.abbreviation')
                    ->label('Listings')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('Last Modified')
                    ->sortable(),
                Tables\Columns\TextColumn::make('published_at')
                    ->label('Published')
                    ->sortable()
                    ->badge()
                    ->default(false)
                    ->formatStateUsing(fn ($state) => $state ? 'Published' : 'Unpublished')
                    ->color(fn ($state) => $state ? 'success' : 'warning'),
                Tables\Columns\TextColumn::make('revised')
                    ->sortable()
                    ->formatStateUsing(fn ($state) => $state ? 'Revised' : 'Up to date')
                    ->badge()
                    ->color(fn ($state) => $state ? 'warning' : 'success'),
            ])
            ->filters([
                Filter::make('unpublished')
                    ->label('Unpublished')
                    ->query(fn (Builder $query) => $query->whereNull('published_at')),
                Filter::make('revised')
                    ->label('Revised')
                    ->query(fn (Builder $query) => $query->where('revised', true)),
                SelectFilter::make('department')
                    ->relationship('listings.department', 'abbreviation')
                    ->preload(),
            ])
            ->actions([
                Tables\Actions\ActionGroup::make([
                    Tables\Actions\Action::make('Edit')
                        ->url(fn (Person $record) => route('user.appointment.list', $record)),
                   Tables\Actions\Action::make('Revisions')
                       ->url(fn (Person $record) => Pages\Revisions::getUrl([$record])),
                    Tables\Actions\Action::make('Unpublish')
                        ->url(fn (Person $record) => route('admin.person.unpublish', $record)),
                    Tables\Actions\Action::make('Publish')
                        ->url(fn (Person $record) => route('admin.person.publish', $record)),
                    Tables\Actions\Action::make('Delete')
                        ->url(fn (Person $record) => route('admin.person.delete', $record)),
                ])
            ])
            ->bulkActions([
                //
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPeople::route('/'),
            'revisions' => Pages\Revisions::route('/{record}/revisions'),
        ];
    }
}
