<?php

namespace App\Filament\Resources\Person\Pages;

use App\Filament\Resources\Person\Actions\AddAction;
use App\Filament\Resources\PersonResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListPeople extends ListRecords
{
    protected static string $resource = PersonResource::class;

    protected function getHeaderActions(): array
    {
        return [
            AddAction::make(),
        ];
    }
}
