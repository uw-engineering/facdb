<?php

namespace App\Filament\Resources\Person\Pages;

use App\Filament\Resources\PersonResource;
use Filament\Resources\Pages\Concerns\InteractsWithRecord;
use Filament\Resources\Pages\Page;

class Revisions extends Page
{
    use InteractsWithRecord;

    protected static string $resource = PersonResource::class;
    protected static string $view = 'filament.resources.person-resource.pages.revisions';

    public function getTitle(): string
    {
        return 'Revisions for ' . $this->record->name;
    }

    // protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public function mount(int | string $record): void
    {
        $this->record = $this->resolveRecord($record);
    }

    // public static function getNavigationLabel(): string
    // {
    //     return 'Revisions';
    // }

    // public function form(Form $form): Form
    // {
    //     return $form
    //         ->schema([
    //             Forms\Components\TextInput::make('type')
    //                 ->required()
    //                 ->maxLength(255),
    //         ]);
    // }

    // public function table(Table $table): Table
    // {
    //     return $table
    //         ->recordTitleAttribute('type')
    //         ->columns([
    //             Tables\Columns\TextColumn::make('type'),
    //         ])
    //         ->filters([
    //             //
    //         ])
    //         ->headerActions([
    //             Tables\Actions\CreateAction::make(),
    //         ])
    //         ->actions([
    //             Tables\Actions\ViewAction::make(),
    //             Tables\Actions\EditAction::make(),
    //             Tables\Actions\DeleteAction::make(),
    //         ])
    //         ->bulkActions([
    //             Tables\Actions\BulkActionGroup::make([
    //                 Tables\Actions\DeleteBulkAction::make(),
    //             ]),
    //         ]);
    // }
}
