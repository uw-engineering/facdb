<?php

namespace App\Filament\Resources\Person\Actions;
use App\Services\DirectoryService;
use Filament\Actions\Action;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;

class AddAction
{
    public static function make(): Action
    {
        return Action::make('Add Person')
            ->form([
                TextInput::make('uwnetid')
                    ->label('NetID')
                    ->required(),
            ])
            ->action(function (array $data, DirectoryService $directory): void {
                if ($person = $directory->find($data['uwnetid'])) {
                    redirect()->route('user.basic.name', $person);
                } else {
                    Notification::make('NetId not found')
                        ->type('error')
                        ->show();
                }
            });
    }
}
