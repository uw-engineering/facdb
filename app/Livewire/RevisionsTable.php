<?php

namespace App\Livewire;

use App\Models\Person;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Resources\Pages\Concerns\InteractsWithRecord;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\View\View;
use Livewire\Component;

class RevisionsTable extends Component implements HasForms, HasTable
{
    use InteractsWithForms;
    use InteractsWithTable;

    public Model $record;
    public bool $oldRevisions = false;

    public function mount(Model $record, bool $old = false): void
    {
        $this->record = $record;
        $this->oldRevisions = $old;
    }

    public function table(Table $table): Table
    {
        return $table
            ->relationship(fn (): MorphMany => $this->oldRevisions ?
                $this->record->oldRevisions() : $this->record->revisions())
            //->query()
            ->columns([
                TextColumn::make('key')
                    ->label('Field'),
                TextColumn::make('old_value'),
                TextColumn::make('new_value'),
                TextColumn::make('updated_at'),
            ])
            ->filters([
                //
            ]);
            //->pagination(false);
    }

    public function render(): View
    {
        return view('livewire.revisions-table');
    }
}
