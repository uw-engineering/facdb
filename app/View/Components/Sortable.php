<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Ramsey\Uuid\Uuid;

class Sortable extends Component
{
    /**
     * Generate an html id.
     *
     * @var string $id
     */
    public $id;

    /**
     * Html form name.
     *
     * @var string $name
     */
    public $name;

    /**
     * The data list to make sortable.
     *
     * @var \Illuminate\Support\Collection $list
     */
    public $list;

    /**
     * Label for the html field.
     *
     * @var string $label
     */
    public $label;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($list, $name, $label = '')
    {
        $this->list = $list;
        
        // Ensure there is always an empty element.
        if (!$this->list || count($this->list) == 0) {
            $this->list = collect(['']);
        }

        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.sortable');
    }
}
