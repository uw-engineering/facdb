<?php

namespace App\Console\Commands;

use App\Models\Building;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class ImportUWBuildingsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:buildings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import UW Buildings into database';

    protected $url = 'https://www.washington.edu/students/reg/buildings.html';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $buildings = [];

        $html = file_get_contents($this->url);
        $doc = new \DOMDocument();
        
        libxml_use_internal_errors(true);
        $doc->loadHTML($html);
        libxml_use_internal_errors(false);

        $nodes = $doc->getElementsByTagName('code');

        foreach ($nodes as $node) {
            $building = new Building();
            $building->code = $node->childNodes[0]->data;

            $a = $node->nextSibling->nextSibling;
            $text = '';
            if ($a->tagName == 'a') {
                $text = $a->childNodes[0]->data;
            } else {
                $text = substr($node->nextSibling->data, 3);
            }

            $matches = [];
            if (preg_match('/(.*)(\((.*)\)).*/', $text, $matches)) {
                $building->name = $matches[1];
                $building->details = $matches[3];
            } else {
                $building->name = $text;
            }
            $buildings[] = $building;
        }

        if (!empty($buildings)) {

            DB::table('buildings')->truncate();

            foreach ($buildings as $building) {
                $building->save();
            }

            $this->line('Imported ' . count($buildings) . ' buildings.');
        } else {
            $this->line('Something went wrong. No data was changed.');
        }
    }
}
