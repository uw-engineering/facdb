<?php

namespace App\Console\Commands;

use App\{Person, Listing, Department, Photo, Appointment, ResearchArea};

use Carbon\Carbon;

use Illuminate\Support\Facades\{DB, Storage};
use Illuminate\Console\Command;

class MigrateFacultyCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faclookup:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from the faclookup database';

    protected $url = "https://www.engr.washington.edu/faclookup/inc/images/photo_thumbs";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $people = DB::connection('faclookup')->table('person')->get();

        $this->line("There are ". $people->count() . " records to migrate.");
        $this->line("Please be patient, this will take some time.");

        // Loop through every person
        foreach ($people as $person) {
            // Create person object and map data
            $newPerson = new Person();
            $newPerson->first_name = $person->first_name;
            $newPerson->middle_name = $person->middle_initial;
            $newPerson->last_name = $person->last_name;
            $newPerson->employee_id = $person->empl_id;
            $newPerson->email = $person->work_email;
            $newPerson->phone = $person->work_phone;
            $newPerson->fax = $person->work_fax;
            $newPerson->website_url = $person->url_homepage;
            $newPerson->save();

            $photoUrl = $this->url . "/{$person->p_id}.jpg";
            try {
                $photoBytes = file_get_contents($photoUrl);
                $photo = Photo::createFromByteString(
                    $photoBytes,
                    ['mime' => 'image/jpg']
                );
                $newPerson->files()->save($photo, ['usage' => 'photo']);
            } catch (\Exception $e) {
                // ha
            }

            // Grab person department info
            $personDepts = DB::connection('faclookup')
                ->table('person_dept')
                ->where('p_id', $person->p_id)
                ->join('department', 'person_dept.d_id', 'department.d_id')
                ->select('person_dept.position_title', 'department.name')
                ->get();

            // Grab the person research term info
            $terms = DB::connection('faclookup')
                ->table('person_research')
                ->select('keyword')
                ->join('research_area', 'person_research.r_id', 'research_area.r_id')
                ->where('p_id', $person->p_id)
                ->get();

            // Create a listing for each listed department
            foreach ($personDepts as $personDept) {
                $department = Department::where('name', $personDept->name)->first();
                
                $appointment = new Appointment();
                $appointment->title = $personDept->position_title;
                $appointment->person()->associate($newPerson);
                if ($department !== null) {
                    $appointment->department()->associate($department);

                    try {
                        $listing = new Listing();
                        $listing->department()->associate($department);
                        $listing->person()->associate($newPerson);
                        $listing->save();

                        foreach ($terms as $term) {
                            $area = ResearchArea::firstOrCreate(['name' => ucwords($term->keyword)]);
                            $listing->personalResearchAreas()->attach($area);
                        }
                    } catch (\Exception $e) {
                        //
                    }

                } else {
                    $appointment->other_department = $personDept->name;
                }
                $appointment->save();
            }

            if ($person->display) {
                $newPerson->published_at = Carbon::now();
                $newPerson->save();
            }
        }

        $this->line("Done!");
    }
}
