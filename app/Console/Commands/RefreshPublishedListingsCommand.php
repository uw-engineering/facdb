<?php

namespace App\Console\Commands;

use App\Models\Person;
use App\Models\PublishedPerson;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class RefreshPublishedListingsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listings:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh all of the published non-revised listings with latest data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // These published people have already been deleted
        foreach (PublishedPerson::doesntHave('personObject')->get() as $publishedPerson) {
            $publishedPerson->departments()->detach();
            $publishedPerson->delete();
        }

        Person::chunk(20, function ($people) {
            foreach ($people as $person) {
                if ($person->published_at && !$person->revised) {
                    $person->publish();
                }
            }
        });
    }
}
