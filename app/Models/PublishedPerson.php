<?php

namespace App\Models;

use App\Events\PersonPublished;
use App\Events\PersonUnpublished;
use Illuminate\Database\Eloquent\Model;

class PublishedPerson extends Model
{
    protected $fillable = ['person_id', 'person', 'hash', 'name', 'last_name', 'research'];

    protected $visible = ['updated_at', 'person', 'hash', 'name', 'last_name', 'research'];

    protected $casts = [
        'person' => 'json',
    ];

    protected $dispatchesEvents = [
        'created' => PersonPublished::class,
        'updated' => PersonPublished::class,
        'deleted' => PersonUnpublished::class,
    ];

    public function personObject()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class)->withPivot('adjunct');
    }
}
