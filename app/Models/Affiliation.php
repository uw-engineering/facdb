<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Znck\Eloquent\Traits\BelongsToThrough;

class Affiliation extends Model
{
    use SoftDeletes, HasFactory, HasRevisions, BelongsToThrough;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'listing_id'
    ];

    public function getRevisedAttribute()
    {
        return $this->revisions->count() !== 0;
    }

    /**
     * Get the listing for this affiliation.
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class);
    }

    public function person(): \Znck\Eloquent\Relations\BelongsToThrough
    {
        return $this->belongsToThrough(Person::class, Listing::class);
    }

    public function researchAreas()
    {
        return $this->belongsToMany(ResearchArea::class, 'affiliation_research_area');
    }
}
