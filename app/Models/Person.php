<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\PublishedPerson;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use UWCoE\FileDB\Traits\ReferencesFiles;
use Hashids\Hashids;

class Person extends Model
{
    use HasFactory, SoftDeletes, ReferencesFiles, HasRevisions;

    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'employee_id',
        'reg_id',
        'published_at',
        'files',
        'photo',
        'revisionHistory',
        'appointmentsWithTrashed',
        'listingsWithTrashed',
    ];
    protected $appends = ['photo_url', 'photo_id', 'full_name', 'cv_url'];
    protected $dontKeepRevisionOf = ['published_at', 'revised'];
  
    protected $casts = [
        'title' => 'array',
        'social' => 'array',
        'revised' => 'boolean',
        'published_at' => 'datetime',
    ];

    public static $snakeAttributes = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->revisionCreationsEnabled = false;
    }

    public function unpublish()
    {
        $this->published_at = null;
        $this->save();

        $publishedPerson = PublishedPerson::where('person_id', $this->id)->first();
        if ($publishedPerson) {
            $publishedPerson->departments()->detach();
            $publishedPerson->delete();
        }
    }

    public function publish()
    {
        $this->published_at = Carbon::now();
        $this->revised = false;
        $this->save();

        // Make sure we have all the relationship data loaded
        $this->load([
            'listings',
            'listings.department',
            'listings.projects',
            'listings.affiliations',
            'listings.affiliations.researchAreas',
            'listings.strategicResearchAreas',
            'listings.departmentResearchAreas',
            'listings.personalResearchAreas',
            'appointments',
            'appointments.department',
        ]);

        $attempts = 1;
        do {
            try {
                $researchAreas = [];
                foreach ($this->listings as $listing) {
                    foreach ($listing->departmentResearchAreas as $departmentResearchArea) {
                        $researchAreas[] = $departmentResearchArea->name;
                    }
                    foreach ($listing->strategicResearchAreas as $strategicResearchArea) {
                        $researchAreas[] = $strategicResearchArea->name;
                    }
                }

                $hash = preg_replace('/[\."]/', '', str_replace(' ', '-', strtolower(trim($this->full_name))))
                    . ($attempts != 1 ? $attempts : '');

                $names = explode(' ', $this->full_name);

                $publishedPerson = PublishedPerson::updateOrCreate(
                    ['person_id' => $this->id],
                    [
                        'person' => $this,
                        'hash' => $hash,
                        'name' => $this->full_name,
                        'last_name' => $names[count($names) - 1],
                        'research' => implode(',', $researchAreas)
                    ]
                );
            } catch (\Illuminate\Database\QueryException $e) {
                $attempts++;
                continue;
            }
            break;
        } while ($attempts < 10);

        $dept_ids = [];
        foreach ($this->appointments as $appointment) {
            if ($appointment->department_id) {
                $dept_ids[$appointment->department_id] = ['adjunct' => ($appointment->adjunct ? 1 : 0)];
            }
        }

        //$dept_ids = $this->appointments->pluck('department_id')->filter();

        $publishedPerson->departments()->sync($dept_ids);
    }

    protected function updatePublishedPerson($person, $attempt)
    {

    }

    public function getFullNameAttribute()
    {
        $fullName = $this->first_name;
        if ($this->middle_name) {
            $fullName .= ' ' . $this->middle_name;
        }
        $fullName .= ' ' . $this->last_name;
        return $fullName;
    }

    /**
     * Get the listings for this Person.
     */
    public function listings()
    {
        return $this->hasMany(Listing::class);
    }
    public function listingsWithTrashed()
    {
        return $this->hasMany(Listing::class)->withTrashed();
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
    public function appointmentsWithTrashed()
    {
        return $this->hasMany(Appointment::class)->withTrashed();
    }

    public function publishedPerson()
    {
        return $this->hasOne(PublishedPerson::class);
    }

    public function canCreateListing()
    {
        return $this->departmentsAvailableToList()->count();
    }

    public function departmentsAvailableToList()
    {
        $listingDepts = $this->listings->pluck('department');
        return $this->appointments->pluck('department')->diff($listingDepts)->filter();
    }

    public function otherListings($listing)
    {
        return $this->listings->filter(function ($val) use ($listing) {
            return $val->id != $listing->id;
        });
    }

    public function getStatusAttribute()
    {
        if (!$this->published_at) {
            return 'unpublished';
        } else {
            return $this->revised ? 'revisions' : 'published';
        }
    }

    public function getPhotoAttribute()
    {
        $file = $this->files
            ->filter(fn ($file) => $file->pivot->usage == 'photo')
            ->sortBy('pivot.created_at')
            ->last();
        if ($file) {
            $photo = new Photo();
            $photo->fillFromFile($file);
            return $photo;
        }
        return null;
    }

    public function getPhotoUrlAttribute()
    {
        if ($this->photo) {
            return $this->photo->url;
        } else {
            return route('default-photo');
        }
    }

    public function getPhotoIdAttribute()
    {
        return $this->photo->id ?? null;
    }

    public function getCvAttribute()
    {
        $file = $this->files()
            ->wherePivot('usage', 'cv')
            ->first();

        if ($file) {
            $cv = new Cv();
            $cv->fillFromFile($file);
            return $cv;
        }

        return null;
    }

    public function cvs()
    {
        return $this->files()->wherePivot('usage', 'cv');
    }

    public function getCvUrlAttribute()
    {
        return $this->cv->url ?? null;
    }

    // public function photoUrl(int $width = 46, int $height = 0): string
    // {
    //     if ($width && !$height) {
    //         $height = $width;
    //     }
    //     if ($this->photo) {
    //         return route('photo', [$this->photo, $width, $height]);
    //     } else {
    //         return route('default-photo', [$width, $height]);
    //     }
    // }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public static function fromNetid($uwnetid)
    {
        return static::where('uwnetid', '=', $uwnetid)->firstOrCreate(['uwnetid' => $uwnetid]);
    }

    public static function fromAuth()
    {
        // Allow admins to take masquerade as users for editing
        // $masqueradeAs = session('masquerade');
        
        // if ($masqueradeAs) {
        //     return static::findOrFail($masqueradeAs);
        // }

        return static::fromNetid(Auth::user()->uwnetid);
    }

    /**
     * Get the route key for the model.
     */
    public function getRouteKeyName(): string
    {
        return 'hash';
    }

    public function generateHash(): string
    {
        return (new Hashids(config('app.key'), 10))->encode($this->getKey());
    }

    // public static function registerRouteBinding()
    // {
    //     Route::bind('person', function($value, $route) {
    //         $hashids = (new Hashids(config('app.key'), 10))->decode($value);
    //         //return !empty($hashids) ? Photo::findOrFail($hashids[0]) : null;
    //         return !empty($hashids) ? static::where('id', $hashids[0])->firstOrFail() : static::fromAuth();
    //     });
    // }
}
