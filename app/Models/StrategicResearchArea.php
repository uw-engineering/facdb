<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StrategicResearchArea extends Model
{
    use SoftDeletes, HasFactory, HasRevisions;

    protected $fillable = ['research_area_id'];

    /**
     * Get the listing for this research area.
     */
    public function researchArea()
    {
        return $this->belongsTo(ResearchArea::class);
    }
}
