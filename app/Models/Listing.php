<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Events\ModelSynced;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model
{
    use HasRevisions, HasFactory, SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'person_id',
        'department_id',
        'projectsWithTrashed',
        'affiliationsWithTrashed',
        'revisionHistory'
    ];

    protected $appends = [
        'education'
    ];
    
    protected $casts = [
        'students' => 'array',
        'courses' => 'array',
        'awards' => 'array',
        'fellowships' => 'array',
        'memberships' => 'array',
        'publications' => 'array',
        'prior_appointments' => 'array',
        'doctorate' => 'array',
        'masters' => 'array',
        'bachelors' => 'array',
    ];

    public function syncResearchAreas(string $type, array $ids)
    {
        $original = $this->{$type.'ResearchAreas'}->pluck('id')->all();
        $remove = [];
        $add = [];
        foreach ($this->{$type.'ResearchAreas'} as $area) {
            if (!in_array($area->id, $ids)) {
                $remove[$area->pivot->id] = $area->id;
            }
        }

        $this->{$type.'ResearchAreas'}()->sync($ids);
        $this->load($type.'ResearchAreas');

        foreach ($this->{$type.'ResearchAreas'} as $area) {
            if (!in_array($area->id, $original)) {
                $add[$area->pivot->id] = $area->id;
            }
        }

        $changes = ['add' => $add, 'remove' => $remove];

        event(new ModelSynced('listing_'.$type.'_research_area', $changes));
    }

    public function getRevisedAttribute()
    {
        $revised = $this->revisions->count() !== 0;

        if ($revised) {
            return true;
        }

        foreach ($this->affiliationsWithTrashed as $affiliation) {
            if ($affiliation->revised) {
                return true;
            }
        }

        foreach ($this->projectsWithTrashed as $project) {
            if ($project->revised) {
                return true;
            }
        }

        return false;
    }

    public function getEducationAttribute()
    {
        return array_merge(
            $this->doctorate ?? [],
            $this->masters ?? [],
            $this->bachelors ?? []
        );
    }

    /**
     * Get the affiliations for this listing.
     */
    public function affiliations()
    {
        return $this->hasMany(Affiliation::class);
    }

    public function affiliationsWithTrashed()
    {
        return $this->affiliations()->withTrashed();
    }

    /**
     * Get the department for this listing.
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
    public function projectsWithTrashed()
    {
        return $this->projects()->withTrashed();
    }

    /**
     * Get the person for this listing.
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * Get the research areas for this listing.
     */
    public function strategicResearchAreas()
    {
        return $this->belongsToMany(ResearchArea::class, 'listing_strategic_research_area')->withPivot('id');
    }

    public function personalResearchAreas()
    {
        return $this->belongsToMany(ResearchArea::class, 'listing_personal_research_area')
            ->withPivot('id', 'order')
            ->orderBy('order');
    }

    public function departmentResearchAreas()
    {
        return $this->belongsToMany(ResearchArea::class, 'listing_department_research_area')->withPivot('id');
    }
}
