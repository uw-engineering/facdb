<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NetId;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'auths';
    protected $guarded = [];

    public function getShibbolethParameters()
    {
        return NetId::all();
    }

    public function isAdmin()
    {
        return true;
    }

    public function getNameAttribute(): string
    {
        return $this->uwnetid;
    }

}
