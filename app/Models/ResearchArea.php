<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResearchArea extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $visible = ['name'];

    /**
     * Get the listing for this research area.
     */
    public function strategicResearchArea()
    {
        return $this->hasOne(StrategicResearchArea::class);
    }

    public function departmentResearchArea()
    {
        return $this->hasOne(DepartmentResearchArea::class);
    }
}
