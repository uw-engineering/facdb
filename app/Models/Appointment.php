<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes, HasFactory, HasRevisions;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'department_id',
        'person_id',
    ];

    public function getRevisedAttribute()
    {
        return $this->revisions->count() !== 0;
    }

    /**
     * Get the listing for this research area.
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
