<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\SoftDeletes;
use UWCoE\FileDB\File;

class Photo extends File
{
    use HasRevisions, SoftDeletes;

    protected $table = 'files';

    public function people()
    {
        return $this->morphedByMany(Person::class, 'file_reference', 'file_references', 'file_id');
    }

    public function getUrlAttribute()
    {
        $person = $this->people->first();
        if ($person->publishedPerson) {
            return route('profile-photo', [$person->publishedPerson->hash]);
        } else {
            return route('photo', [$this]);
        }
    }
}
