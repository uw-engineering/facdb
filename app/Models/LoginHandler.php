<?php

namespace App\Models;

use UWCoE\NetId\Concerns\HandlesLogin;
use UWCoE\NetId\NetId;
use Auth;

class LoginHandler implements HandlesLogin
{

    public function handle(NetId $netid)
    {
        $uwnetid = $netid->attrs->get('uwnetid');
        $user = User::where('uwnetid', $uwnetid)->first();
        
        if ($user) {
            Auth::login($user);
        } else {
            Auth::setUser(new FacultyUser());
        }
    }

}
