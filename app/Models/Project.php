<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Znck\Eloquent\Traits\BelongsToThrough;

class Project extends Model
{
    use SoftDeletes, HasFactory, HasRevisions, BelongsToThrough;

    protected $visible = ['name', 'description'];

    public function getRevisedAttribute()
    {
        return $this->revisions->count() !== 0;
    }

    /**
     * Get the listing for this research area.
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class);
    }

    public function person(): \Znck\Eloquent\Relations\BelongsToThrough
    {
        return $this->belongsToThrough(Person::class, Listing::class);
    }
}
