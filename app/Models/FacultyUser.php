<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use NetId;

class FacultyUser extends Authenticatable
{
    public function getShibbolethParameters()
    {
        return NetId::all();
    }

    public function isAdmin()
    {
        return false;
    }
}
