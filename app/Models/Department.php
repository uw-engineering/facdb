<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $visible = ['name', 'abbreviation', 'url', 'researchAreas'];

    /**
     * Get the listings for this department.
     */
    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

    public function researchAreas()
    {
        return $this->belongsToMany(ResearchArea::class, 'department_research_areas');
    }

    public function publishedPeople()
    {
        return $this->belongsToMany(PublishedPerson::class);
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'abbreviation';
    }

    // public function getRouteKey()
    // {
    //     return $this->abbreviation;
    // }
}
