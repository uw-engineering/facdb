<?php

namespace App\Models\Traits;
use App\Models\Person;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Venturecraft\Revisionable\RevisionableTrait;

trait HasRevisions
{
    use RevisionableTrait;

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;

    public function setRevisionEnabled($to = true)
    {
        $this->revisionEnabled = $to;
    }

    public function revisions(): MorphMany
    {
        return $this->buildRevisionRelationship('<');
    }

    public function oldRevisions(): MorphMany
    {
        return $this->buildRevisionRelationship('>=');
    }

    /**
     * Check if our model is a person or related to a person in order to grab
     * the correct published_at column.
     */
    private function buildRevisionRelationship(string $when): MorphMany
    {
        return $this->revisionHistory()
            ->whereHasMorph('revisionable', [static::class], fn ($query) =>
                static::class === Person::class
                ? $query->whereColumn('people.published_at', $when, 'revisions.updated_at')
                : $query->whereRelation('person', fn ($q2) => 
                    $q2->whereColumn('people.published_at', $when, 'revisions.updated_at'))
            );
    }
}
