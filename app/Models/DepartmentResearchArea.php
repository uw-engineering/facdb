<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentResearchArea extends Model
{
    use HasFactory;

    protected $fillable = ['research_area_id', 'department_id'];

    /**
     * Get the listing for this research area.
     */
    public function researchArea()
    {
        return $this->belongsTo(ResearchArea::class);
    }

    public function department()
    {
        return $this->hasOne(Department::class);
    }
}
