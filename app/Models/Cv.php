<?php

namespace App\Models;

use App\Models\Traits\HasRevisions;
use Illuminate\Database\Eloquent\SoftDeletes;
use UWCoE\FileDB\File;

class Cv extends File
{
    use SoftDeletes, HasRevisions;

    protected $table = 'files';

    protected $guarded = [];

    protected $appends = ['person'];


    public function people()
    {
        return $this->morphedByMany(Person::class, 'file_reference', 'file_references', 'file_id');
            //->withPivot('usage');
    }

    public function getPersonAttribute()
    {
        return $this->people()->latest()->first();
    }

    public function getUrlAttribute()
    {
        $person = $this->people->first();
        if ($person->publishedPerson ?? false) {
            return route('cv', [$person->publishedPerson->hash]);
        } else {
            return null;
        }
    }
}
