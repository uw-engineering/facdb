<?php

namespace App\Services;
use UWCoE\PWS\Directory;
use UWCoE\PWS\Person as PersonData;
use App\Models\Person;

class DirectoryService
{
    public function __construct(
        protected Directory $directory,
    ) {}

    public function find(string $uwnetid): ?Person
    {
        if ($data = $this->directory->fromNetId($uwnetid)) {
            return $this->getPerson($data);
        }

        return null;
    }

    protected function getPerson(PersonData $data): Person
    {       
        $person = Person::firstOrNew(['reg_id' => $data->UWRegID]);
            
        $person->uwnetid = $data->UWNetID;
        $person->first_name = $data->PreferredFirstName ? $data->PreferredFirstName : current(explode(' ', $data->RegisteredName));
        $person->last_name = $data->PreferredSurname ? $data->PreferredSurname : $data->RegisteredSurname;
        $person->middle_name = $data->PreferredMiddleName ? $data->PreferredMiddleName : null;
        $person->email = $data->UWNetID . '@uw.edu';
        $person->employee_id = $data->PersonAffiliations?->EmployeePersonAffiliation?->EmployeeID;
        $person->title = [];
        $person->save();

        return $person;
    }
}
