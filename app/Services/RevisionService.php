<?php

namespace App\Services;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;

class RevisionService
{
    public function __construct(
        protected Request $request,
        protected DatabaseManager $db,
    ) {}

    public function addRevision($key, $model, $revisionable_type, $revisionable_id, $old, $new): void
    {
        $this->db->table('revisions')->insert([
            'revisionable_type' => $revisionable_type,
            'revisionable_id' => $revisionable_id,
            'user_id' => $this->request->user()->id,
            'key' => $key,
            'old_value' => $old,
            'new_value' => $new,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $model->revised = true;
        $model->save();
    }
}
