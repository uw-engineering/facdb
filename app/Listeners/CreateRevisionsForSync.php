<?php

namespace App\Listeners;

use App\Events\ModelSynced;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateRevisionsForSync
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(ModelSynced $event)
    {
        $this->addRevisions($event->pivot, $event->add);
        $this->removeRevisions($event->pivot, $event->remove);
    }

    protected function addRevisions($pivot, $changes)
    {
        foreach ($changes as $pivot_id => $thing_id) {
            DB::table('revisions')->insert([
                'revisionable_type' => 'Pivot:'.$pivot,
                'revisionable_id' => $pivot_id,
                'user_id' => Auth::user()->id,
                'key' => '',
                'old_value' => null,
                'new_value' => $thing_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }

    protected function removeRevisions($pivot, $changes)
    {
        foreach ($changes as $pivot_id => $thing_id) {
            DB::table('revisions')->insert([
                'revisionable_type' => 'Pivot:'.$pivot,
                'revisionable_id' => $pivot_id,
                'user_id' => Auth::user()->id,
                'key' => '',
                'old_value' => $thing_id,
                'new_value' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
