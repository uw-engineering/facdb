<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use Illuminate\Http\Request;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        // View::composer('*', function ($view) {

        //     $userMenu = [
        //         [
        //             'name' => 'Basic Info',
        //             'route' => 'user.welcome',
        //             'icon' => 'id-card-o'
        //         ],
        //         [
        //             'name' => 'Export',
        //             'route' => 'user.welcome',
        //             'icon' => 'floppy-o'
        //         ],
        //         [
        //             'name' => 'Tools',
        //             'route' => 'user.welcome',
        //             'icon' => 'cog'
        //         ]
        //     ];

        //     $adminMenu = [
        //         [
        //             'name' => 'History',
        //             'route' => 'admin.history',
        //             'icon' => 'cog'
        //         ]
        //     ];

        //     $menu = in_array('admin', Route::current()->computedMiddleware) ? $adminMenu : $userMenu;
        //     $view->with('menu', $menu);
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
