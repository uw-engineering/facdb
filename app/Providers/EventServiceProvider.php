<?php

namespace App\Providers;

use App\Events\ModelSynced;
use App\Listeners\CreateRevisionsForSync;
use App\Models\Person;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ModelSynced::class => [
            CreateRevisionsForSync::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('revisionable.*', function($event, $data) {
            // Do something with the revisions or the changed model. 

            $person = $data['model'];

            if (get_class($person) != Person::class) {
                $person = $person->person ?? ($person->listing->person ?? null);
            }

            if ($person && get_class($person) == Person::class) {
                $person->setRevisionEnabled(false);
                $person->revised = true;
                $person->save();
                $person->setRevisionEnabled(true);
            }
        });
    }
}
