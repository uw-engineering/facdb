<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use App\Models\Person;
use Livewire\Livewire;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        // Set up livewire to work in a subdirectory
        $base = config('app.url_base');
        Livewire::setUpdateRoute(fn ($handle) => Route::post("$base/livewire/update", $handle)->middleware('web'));
        Livewire::setScriptRoute(fn ($handle) => Route::get("$base/livewire/livewire.min.js", $handle));


        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware(['web', 'public'])
                ->namespace($this->namespace . '\User')
                ->name('user.')
                ->group(base_path('routes/user.php'));

            Route::prefix('admin')
                ->middleware(['web', 'admin'])
                ->namespace($this->namespace . '\Admin')
                ->name('admin.')
                ->group(base_path('routes/admin.php'));

            Route::prefix('docs')
                ->namespace($this->namespace . '\Docs')
                ->name('docs.')
                ->group(base_path('routes/docs.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
