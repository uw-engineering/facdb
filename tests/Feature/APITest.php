<?php

namespace Tests\Feature;

use Tests\TestCase;

class APITest extends TestCase
{
    public function test_people(): void
    {
        $response = $this->getJson('/api/people?dept=me');

        $response
            ->assertStatus(200)
            ->assertJson([
                'current_page' => 1
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertHeader('Access-Control-Allow-Origin', '*');
    }

    public function test_people_pagination(): void
    {
        $response = $this->getJson('/api/people?page_size=10');

        $response
            ->assertJson([
                'per_page' => 10
            ])
            ->assertJson([
                'first_page_url' => url('/api/people?page_size=10&page=1')
            ]);

        $response = $this->json('GET', 'api/people');

        $response
            ->assertJson([
                'per_page' => 20
            ]);
    }
}
