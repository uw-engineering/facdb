<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Support\Facades\URL;
use App\Models\Person;

class EditContactTest extends TestCase
{
    public function test_contact(): void
    {
        $person = Person::factory()->create();

        $response = $this
            ->actingAs($this->user)
            ->post('edit/' . $person->hash . '/basic/contact', [
                'email' => 'test@example.com'
            ]);

        $response
            ->assertRedirect(URL::route('user.basic.admin', [
                'person' => $person,
            ]));
    }
}
