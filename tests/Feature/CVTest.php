<?php

namespace Tests\Feature;

use App\Models\Listing;
use App\Models\Person;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CVTest extends TestCase
{   
    public function test_cv_triggers_revision(): void
    {
        $person = Person::factory()->create();
        $listing = Listing::factory()
            ->hasDepartment()
            ->create(['person_id' => $person->id]);

        $listing = $person->listings->first();

        $this->assertFalse($person->revised);

        $this->actingAs($this->user)
            ->post("edit/$person->hash/listing/$listing->id/history/cv", [
                'cv' => UploadedFile::fake()->image('cv.pdf'),
            ]);

        $person->refresh();

        $this->assertTrue($person->revised);

        $person->publish();

        $this->assertFalse($person->revised);

        $this->actingAs($this->user)
            ->post("edit/$person->hash/listing/$listing->id/history/cv", [
                'delete' => true,
            ]);

        $person->refresh();

        $this->assertTrue($person->revised);
    }
}
