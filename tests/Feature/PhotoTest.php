<?php

namespace Tests\Feature;

use App\Models\Person;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PhotoTest extends TestCase
{   
    public function test_photo_triggers_revision(): void
    {
        $person = Person::factory()->create();

        $this->assertFalse($person->revised);

        $this->actingAs($this->user)
            ->post('edit/' . $person->hash . '/basic/photo', [
                'photo' => UploadedFile::fake()->image('photo.jpg'),
            ]);

        $person->refresh();

        $this->assertTrue($person->revised);
    }
}
