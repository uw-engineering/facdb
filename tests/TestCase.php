<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        DB::table('auths')->insert([
            'uwnetid' => 'test',
        ]);
        DB::table('users')->insert([
            'uwnetid' => 'test'
        ]);

        $this->user = User::where('uwnetid', 'test')->first();
    }
}
