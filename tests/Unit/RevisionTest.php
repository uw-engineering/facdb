<?php

namespace Tests\Unit;

use App\Models\Appointment;
use App\Models\Person;
use Tests\TestCase;

class RevisionTest extends TestCase
{   
    public function test_change_triggers_revised(): void
    {
        $person = Person::factory()->create();

        $this->assertFalse($person->revised);

        $person->first_name = '---';
        $person->save();

        $this->assertTrue($person->revised);
    }

    public function test_publishing_sets_revised_to_false(): void
    {
        $person = Person::factory()->create();

        $person->first_name = '---';
        $person->save();
        $person->unpublish();
        $person->refresh();

        $this->assertTrue($person->revised);

        $person->publish();

        $this->assertFalse($person->revised);

        $person->appointments()->save(Appointment::factory()->make());
        $person->refresh();

        $this->assertTrue($person->revised);

        $person->publish();

        $this->assertFalse($person->revised);
    }
}
