<?php

namespace Tests\Unit;

use App\Services\DirectoryService;
use Mockery\MockInterface;
use Tests\TestCase;
use UWCoE\PWS\Directory;
use UWCoE\PWS\Person;

class DirectoryServiceTest extends TestCase
{
    public function test_find(): void
    {
        $this->mock(Directory::class, function (MockInterface $mock) {
            $mock->shouldReceive('fromNetId')
                ->with('uwnetid')
                ->andReturn(new Person([
                    'UWRegID' => '123456789',
                    'UWNetID' => 'uwnetid',
                ]));
            $mock->shouldReceive('fromNetId')
                ->with('none')
                ->andReturn(null);
        });
        $directory = app(DirectoryService::class);

        $person = $directory->find('uwnetid');

        $this->assertNotNull($person);
        
        $person = $directory->find('none');

        $this->assertNull($person);
    }
}
